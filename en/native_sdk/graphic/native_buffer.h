/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_BUFFER_H_
#define NDK_INCLUDE_NATIVE_BUFFER_H_

/**
 * @addtogroup OH_NativeBuffer
 * @{
 *
 * @brief Provides the capabilities of <b>NativeBuffer</b>. Using the functions provided by this module,
 * you can apply for, use, and release the shared memory, and query its attributes.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

/**
 * @file native_buffer.h
 *
 * @brief Declares the functions for obtaining and using <b>NativeBuffer</b>.
 *
 * File to include: <native_buffer/native_buffer.h>
 * @library libnative_buffer.so
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Provides the declaration of an <b>OH_NativeBuffer</b> struct.
 * @since 9
 */
struct OH_NativeBuffer;

/**
 * @brief Provides the declaration of an <b>OH_NativeBuffer</b> struct.
 * @since 9
 */
typedef struct OH_NativeBuffer OH_NativeBuffer;

/**
 * @brief Enumerates the <b>OH_NativeBuffer</b> usages.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 10
 * @version 1.0
 */
enum OH_NativeBuffer_Usage {
    /**
     * Read by the CPU.
     */
    NATIVEBUFFER_USAGE_CPU_READ = (1ULL << 0),
    /**
     * Write by the CPU.
     */
    NATIVEBUFFER_USAGE_CPU_WRITE = (1ULL << 1),
    /**
     * Direct memory access to the buffer.
     */
    NATIVEBUFFER_USAGE_MEM_DMA = (1ULL << 3),
};

/**
 * @brief Enumerates the <b>OH_NativeBuffer</b> formats.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 10
 * @version 1.0
 */
enum OH_NativeBuffer_Format {
    /**
     * RGB565.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_565 = 3,
    /**
     * RGBA5658.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_5658,
    /**
     * RGBX4444.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_4444,
    /**
     * RGBA4444.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_4444,
    /**
     * RGB444.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_444,
    /**
     * RGBX5551.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_5551,
    /**
     * RGBA5551.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_5551,
    /**
     * RGB555.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_555,
    /**
     * RGBX8888.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_8888,
    /**
     * RGBA8888.
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_8888,
    /**
     * RGB888.
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_888,
    /**
     * BGR565.
     */
    NATIVEBUFFER_PIXEL_FMT_BGR_565,
    /**
     * BGRX4444.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_4444,
    /**
     * BGRA4444.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_4444,
    /**
     * BGRX5551.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_5551,
    /**
     * BGRA5551.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_5551,
    /**
     * BGRX8888.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_8888,
    /**
     * BGRA8888.
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_8888,
    /**
     * Invalid format.
     */
    NATIVEBUFFER_PIXEL_FMT_BUTT = 0X7FFFFFFF
};

/**
 * @brief Enumerates the color spaces of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 11
 * @version 1.0
 */
enum OH_NativeBuffer_ColorSpace {
    /** No color space is available. */
    OH_COLORSPACE_NONE,
    /**
     * The color gamut is BT601_P, the transfer function is BT709, the conversion matrix is BT601_P,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT601_EBU_FULL,
    /**
     * The color gamut is BT601_N, the transfer function is BT709, the conversion matrix is BT601_N,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT601_SMPTE_C_FULL,
    /**
     * The color gamut is BT709, the transfer function is BT709, the conversion matrix is BT709,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT709_FULL,
    /**
     * The color gamut is BT2020, the transfer function is HLG, the conversion matrix is BT2020,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT2020_HLG_FULL,
    /**
     * The color gamut is BT2020, the transfer function is PQ, the conversion matrix is BT2020,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_BT2020_PQ_FULL,
    /**
     * The color gamut is BT601_P, the transfer function is BT709, the conversion matrix is BT601_P,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT601_EBU_LIMIT,
    /**
     * The color gamut is BT601_N, the transfer function is BT709, the conversion matrix is BT601_N,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT601_SMPTE_C_LIMIT,
    /**
     * The color gamut is BT709, the transfer function is BT709, the conversion matrix is BT709,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT709_LIMIT,
    /**
     * The color gamut is BT2020, the transfer function is HLG, the conversion matrix is BT2020,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT2020_HLG_LIMIT,
    /**
     * The color gamut is BT2020, the transfer function is PQ, the conversion matrix is BT2020,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_BT2020_PQ_LIMIT,
    /**
     * The color gamut is SRGB, the transfer function is SRGB, the conversion matrix is BT601_N,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_SRGB_FULL,
    /**
     * The color gamut is P3_D65, the transfer function is SRGB, the conversion matrix is P3,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_P3_FULL,
    /**
     * The color gamut is P3_D65, the transfer function is HLG, the conversion matrix is P3,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_P3_HLG_FULL,
    /**
     * The color gamut is P3_D65, the transfer function is PQ, the conversion matrix is P3,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_P3_PQ_FULL,
    /**
     * The color gamut is ADOBERGB, the transfer function is ADOBERGB, the conversion matrix is ADOBERGB,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_ADOBERGB_FULL,
    /**
     * The color gamut is SRGB, the transfer function is SRGB, the conversion matrix is BT601_N,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_SRGB_LIMIT,
    /**
     * The color gamut is P3_D65, the transfer function is SRGB, the conversion matrix is P3,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_P3_LIMIT,
    /**
     * The color gamut is P3_D65, the transfer function is HLG, the conversion matrix is P3,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_P3_HLG_LIMIT,
    /**
     * The color gamut is P3_D65, the transfer function is PQ, the conversion matrix is P3,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_P3_PQ_LIMIT,
    /**
     * The color gamut is ADOBERGB, the transfer function is ADOBERGB, the conversion matrix is ADOBERGB,
     * and the data range is RANGE_LIMITED.
     */
    OH_COLORSPACE_ADOBERGB_LIMIT,
    /** The color gamut is SRGB, and the transfer function is LINEAR. */
    OH_COLORSPACE_LINEAR_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_LINEAR_SRGB</b>. */
    OH_COLORSPACE_LINEAR_BT709,
    /** The color gamut is P3_D65, and the transfer function is LINEAR. */
    OH_COLORSPACE_LINEAR_P3,
    /** The color gamut is BT2020, and the transfer function is LINEAR. */
    OH_COLORSPACE_LINEAR_BT2020,
    /** It is equivalent to <b>OH_COLORSPACE_SRGB_FULL</b>. */
    OH_COLORSPACE_DISPLAY_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_P3_FULL</b>. */
    OH_COLORSPACE_DISPLAY_P3_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_P3_HLG_FULL</b>.
    OH_COLORSPACE_DISPLAY_P3_HLG,
    /** It is equivalent to <b>OH_COLORSPACE_P3_PQ_FULL</b>. */
    OH_COLORSPACE_DISPLAY_P3_PQ,
    /**
     * The color gamut is BT2020, the transfer function is SRGB, the conversion matrix is BT2020,
     * and the data range is RANGE_FULL.
     */
    OH_COLORSPACE_DISPLAY_BT2020_SRGB,
    /** It is equivalent to <b>OH_COLORSPACE_BT2020_HLG_FULL</b>. */
    OH_COLORSPACE_DISPLAY_BT2020_HLG,
    /** It is equivalent to <b>OH_COLORSPACE_BT2020_PQ_FULL</b>. */
    OH_COLORSPACE_DISPLAY_BT2020_PQ,
};

/**
 * @brief Defines the <b>OH_NativeBuffer</b> attribute configuration, which is used when you apply for
 * a new <b>OH_NativeBuffer</b> instance or query the attributes of an existing instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */
typedef struct {
    /**
     * Width, in pixels.
     */
    int32_t width;
    /**
     * Height, in pixels.
     */
    int32_t height;
    /**
     * Pixel map format.
     */
    int32_t format;
    /**
     * Description of the buffer usage.
     */
    int32_t usage;
    /**
     * Stride of the local window buffer.
     * @since 10
     */
    int32_t stride;
} OH_NativeBuffer_Config;

/**
 * @brief Creates an <b>OH_NativeBuffer</b> instance based on an <b>OH_NativeBuffer_Config</b> struct.
 * A new <b>OH_NativeBuffer</b> instance is created each time this function is called.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param config Pointer to an <b>OH_NativeBuffer_Config</b> instance.
 * @return Returns the pointer to the <b>OH_NativeBuffer</b> instance created if the operation is successful;
 * returns <b>NULL</b> otherwise.
 * @since 9
 * @version 1.0
 */
OH_NativeBuffer* OH_NativeBuffer_Alloc(const OH_NativeBuffer_Config* config);

/**
 * @brief Increases the reference count of an <b>OH_NativeBuffer</b> instance by 1.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Reference(OH_NativeBuffer *buffer);

/**
 * @brief Decreases the reference count of an <b>OH_NativeBuffer</b> instance by 1 and
 * when the reference count reaches 0, destroys the instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unreference(OH_NativeBuffer *buffer);

/**
 * @brief Obtains the attributes of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param config Pointer to an <b>OH_NativeBuffer_Config</b> instance, which is used to
 * receive the attributes of <b>OH_NativeBuffer</b>.
 * @since 9
 * @version 1.0
 */
void OH_NativeBuffer_GetConfig(OH_NativeBuffer *buffer, OH_NativeBuffer_Config* config);

/**
 * @brief Maps the ION memory corresponding to an <b>OH_NativeBuffer</b> instance to the process address space.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param virAddr Double pointer to the address of the virtual memory.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 */

int32_t OH_NativeBuffer_Map(OH_NativeBuffer *buffer, void **virAddr);

/**
 * @brief Unmaps the ION memory corresponding to an <b>OH_NativeBuffer</b> instance from the process address space.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unmap(OH_NativeBuffer *buffer);

/**
 * @brief Obtains the sequence number of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @return Returns the unique sequence number of the <b>OH_NativeBuffer</b> instance.
 * @since 9
 * @version 1.0
 */
uint32_t OH_NativeBuffer_GetSeqNum(OH_NativeBuffer *buffer);

/**
 * @brief Sets the color space for an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer Pointer to an <b>OH_NativeBuffer</b> instance.
 * @param colorSpace Color space to set. The value is obtained from <b>OH_NativeBuffer_ColorSpace</b>.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeBuffer_SetColorSpace(OH_NativeBuffer *buffer, OH_NativeBuffer_ColorSpace colorSpace);

#ifdef __cplusplus
}
#endif

/** @} */
#endif
