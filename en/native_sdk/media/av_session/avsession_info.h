/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_INFO_H
#define OHOS_AVSESSION_INFO_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief Provides APIs for audio and video (AV) media control.
 *
 * The APIs can be used to create, manage, and control media sessions.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_info.h
 *
 * @brief Declares the listeners and callbacks related to AV sessions.
 *
 * @since 9
 * @version 1.0
 */

#include <string>
#include "avmeta_data.h"
#include "avplayback_state.h"
#include "avsession_descriptor.h"
#include "key_event.h"

namespace OHOS::AVSession {
/** AV session death callback. */
using DeathCallback = std::function<void()>;

/**
 * @brief Implements listeners related to AV sessions.
 *
 * @since 9
 * @version 1.0
 */
class SessionListener {
public:
    /**
     * @brief Called when an AV session is created.
     *
     * @param descriptor Indicates the pointer to a session descriptor, which is an {@link AVSessionDescriptor} object.
     * @see OnSessionRelease
     * @since 9
     * @version 1.0
     */
    virtual void OnSessionCreate(const AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief Called when an AV session is released.
     *
     * @param descriptor Indicates the pointer to a session descriptor, which is an {@link AVSessionDescriptor} object.
     * @see OnSessionCreate
     * @since 9
     * @version 1.0
     */
    virtual void OnSessionRelease(const AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief Called when the top session is changed for an AV session.
     *
     * @param descriptor Indicates the pointer to a session descriptor, which is an {@link AVSessionDescriptor} object.
     * @since 9
     * @version 1.0
     */
    virtual void OnTopSessionChange(const AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief Default destructor of the {@code SessionListener}.
     *
     * @since 9
     * @version 1.0
     */
    virtual ~SessionListener() = default;
};

/**
 * @brief Implements callbacks for AV sessions.
 *
 * @since 9
 * @version 1.0
 */
class AVSessionCallback {
public:
    /**
     * @brief Called when the playback is in progress for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPlay() = 0;

    /**
     * @brief Called when the playback is paused for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPause() = 0;

    /**
     * @brief Called when the playback is stopped for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnStop() = 0;

    /**
     * @brief Called when the operation of playing the next track is performed for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPlayNext() = 0;

    /**
     * @brief Called when the operation of playing the previous track is performed for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPlayPrevious() = 0;

    /**
     * @brief Called when the fast forward operation is performed for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnFastForward() = 0;

    /**
     * @brief Called when the rewind operation is performed for this AV session.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnRewind() = 0;

    /**
     * @brief Called when the seek operation is performed for this AV session.
     *
     * @param time Indicates the time to seek to, which is calculated from the beginning of the media asset, in ms.
     * The value must be greater than or equal to <b>0</b>.
     * @since 9
     * @version 1.0
     */
    virtual void OnSeek(int64_t time) = 0;

    /**
     * @brief Called when the operation of setting the playback speed is performed for this AV session.
     *
     * @param speed Indicates the playback speed to set.
     * @since 9
     * @version 1.0
     */
    virtual void OnSetSpeed(double speed) = 0;

    /**
     * @brief Called when the operation of setting the loop mode is performed for this AV session.
     *
     * @param loopMode Indicates the loop mode to set, which must be within the range
     * from {@link AVPlaybackState#LOOP_MODE_SEQUENCE} to {@link AVPlaybackState#LOOP_MODE_SHUFFLE}.
     * @since 9
     * @version 1.0
     */
    virtual void OnSetLoopMode(int32_t loopMode) = 0;

    /**
     * @brief Called when the operation of favoriting the media asset is performed for this AV session.
     *
     * @param mediald Indicates the pointer to the media asset ID.
     * @since 9
     * @version 1.0
     */
    virtual void OnToggleFavorite(const std::string& mediald) = 0;

    /**
     * @brief Called when a system key is pressed for this AV session.
     *
     * @param keyEvent Indicates the pointer to the key event code, which is an {@link MMI::KeyEvent} object.
     * @since 9
     * @version 1.0
     */
    virtual void OnMediaKeyEvent(const MMI::KeyEvent& keyEvent) = 0;

    /**
     * @brief Called when the session output device is changed.
     *
     * @param outputDeviceInfo Indicates the pointer to the output device information,
     * which is an {@link OutputDeviceInfo} object.
     * @since 9
     * @version 1.0
     */
    virtual void OnOutputDeviceChange(const OutputDeviceInfo& outputDeviceInfo) = 0;
    /**
     * @brief Default constructor of the {@code AVSessionCallback}.
     *
     * @since 9
     * @version 1.0
     */
    virtual ~AVSessionCallback() = default;
};

/**
 * @brief Implements callbacks related to the session controller.
 *
 * @since 9
 * @version 1.0
 */
class AVControllerCallback {
public:
    /**
     * @brief Called when this AV session is destroyed.
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnSessionDestroy() = 0;

    /**
     * @brief Called when the AV playback state is changed.
     *
     * @param state Indicates the pointer to the playback state, which is an {@link AVPlaybackState} object.
     * @since 9
     * @version 1.0
     */
    virtual void OnPlaybackStateChange(const AVPlaybackState &state) = 0;

    /**
     * @brief Called when the session metadata is changed.
     *
     * @param data Indicates the pointer to the session metadata, which is an {@link AVMetaData} object.
     * @see AVMetaData
     * @since 9
     * @version 1.0
     */
    virtual void OnMetaDataChange(const AVMetaData &data) = 0;

    /**
     * @brief Called when the activation status of this session is changed.
     *
     * @param isActive Specifies whether the session is activated.
     * @since 9
     * @version 1.0
     */
    virtual void OnActiveStateChange(bool isActive) = 0;

    /**
     * @brief Called when the validity of control commands is changed.
     *
     * @param cmds Indicates the pointer to the list of valid commands, 
     * ranging from {@link #SESSION_CMD_INVALID} to {@link #SESSION_CMD_MAX}.
     * @since 9
     * @version 1.0
     */
    virtual void OnValidCommandChange(const std::vector<int32_t> &cmds) = 0;

    /**
     * @brief Called when the session output device is changed.
     *
     * @param outputDeviceInfo Indicates the pointer to the output device information, 
     * which is an {@link OutputDeviceInfo} object.
     * @since 9
     * @version 1.0
     */
    virtual void OnOutputDeviceChange(const OutputDeviceInfo &outputDeviceInfo) = 0;
	
	/**
     * @brief Default destructor of the {@code AVControllerCallback}.
     *
     * @since 9
     * @version 1.0
     */
    virtual ~AVControllerCallback() = default;
};

/**
 * @brief Describes the information about a session token.
 *
 * @since 9
 * @version 1.0
 */
struct SessionToken {
    /** Session ID. */
    std::string sessionId;
    /** Process ID of the session. */
    pid_t pid;
    /** User ID. */
    uid_t uid;
};

/**
 * @brief Enumerates the session data categories.
 *
 * @since 9
 * @version 1.0
 */
enum SessionDataCategory {
    /** Invalid value, which is used internally to determine whether a category is valid. */
    SESSION_DATA_CATEGORY_INVALID = -1,
    /** Session metadata. */
    SESSION_DATA_META = 0,
    /** Session playback state. */
    SESSION_DATA_PLAYBACK_STATE = 1,
    /** Session control command. */
    SESSION_DATA_CONTROL_COMMAND = 2,
    /** Number of session data categories. */
    SESSION_DATA_CATEGORY_MAX = 3,
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_INFO_H
