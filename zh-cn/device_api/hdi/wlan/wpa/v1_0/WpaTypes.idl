/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @添加到组 Wpa
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 您可以使用API启用或禁用WLAN热点、扫描热点、连接到WLAN热点,管理WLAN芯片、网络设备和电源,并申请、释放和移动网络数据缓冲区.
 *
 * @since 4.1
 * @version 1.0
 */

 /**
 * @file WpaTypes.idl
 *
 * @brief 定义与WLAN模块相关的数据类型.
 *
 * WLAN模块数据包括{@code-Feature}对象信息、站（STA）信息等,扫描信息和网络设备信息.
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @brief 定义WLAN模块接口的包路径.
 *
 * @since 4.1
 * @version 1.0
 */
package ohos.hdi.wlan.wpa.v1_0;

/**
 * @brief 定义｛@code Feature｝对象信息.
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiFeatureInfo {
    /** NIC name of the {@code Feature} object. */
    String ifName;
    /** {@code Feature} object. */
    int type;
};

/**
 * @brief 定义Wi-Fi的Wifi状态信息.
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWifiStatus {
    /** 要扫描的基本服务集标识符（BSSID）. */
    unsigned char[] bssid;
    int freq;
    /** SSID to scan. */
    String ssid;
    /** Length of the SSID. */
    int ssidLen;
    String keyMgmt;
    int keyMgmtLen;
    unsigned char[] address;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWifiWpaNetworkInfo {
    int id;
    unsigned char[] ssid;
    unsigned char[] bssid;
    unsigned char[] flags;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWifiWpsParam {
    int anyFlag;
    int multiAp;
    unsigned char[] bssid;
    unsigned char[] pinCode;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaCmdStatus {
    unsigned char[] bssid;
    int freq;
    unsigned char[] ssid;
    int id;
    unsigned char[] keyMgmt;
    unsigned char[] address;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaDisconnectParam {
    unsigned char[] bssid;
    int reasonCode;
    int locallyGenerated;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaConnectParam {
    unsigned char[] bssid;
    int networkId;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaBssidChangedParam {
    unsigned char[] bssid;
    unsigned char[] reason;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaStateChangedParam {
    int status;
    unsigned char[] bssid;
    int networkId;
    unsigned char[] ssid;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaTempDisabledParam {
    int networkId;
    unsigned char[] ssid;
    int authFailures;
    int duration;
    unsigned char[] reason;
};

/**
 * @brief 定义Wi-Fi的Wpa状态信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaAssociateRejectParam {
    unsigned char[] bssid;
    int statusCode;
    int timeOut;
};

/**
 * @brief 定义Wi-Fi的接收扫描结构信息
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaRecvScanResultParam {
    unsigned int scanId;
};

/**
 *  @brief 枚举Wifi技术
 */
enum WifiTechnology {
    UNKNOWN_TECHNOLOGY = 0,
    /**
     * For 802.11a/b/g
     */
    LEGACY = 1,
    /**
     * For 802.11n
     */
    HT = 2,
    /**
     * For 802.11ac
     */
    VHT = 3,
    /**
     * For 802.11ax
     */
    HE = 4,
};

/**
 * @brief 枚举通道工作宽度(以Mhz为单位).
 */
enum WifiChannelWidthInMhz {
  WIDTH_20 = 0,
  WIDTH_40 = 1,
  WIDTH_80 = 2,
  WIDTH_160 = 3,
  WIDTH_80P80 = 4,
  WIDTH_5 = 5,
  WIDTH_10 = 6,
  WIDTH_INVALID = -1
};

/**
 *@brief 枚举传统网络的详细网络模式
 */
enum LegacyMode {
    UNKNOWN_MODE = 0,
    /**
     * For 802.11a
     */
    A_MODE = 1,
    /**
     * For 802.11b
     */
    B_MODE = 2,
    /**
     * For 802.11g
     */
    G_MODE = 3,
};

/**
 * 当前网络和设备支持的连接功能
 */
struct ConnectionCapabilities {
    /**
     * Wifi技术
     */
    WifiTechnology technology;
    /**
     * 信道带宽
     */
    WifiChannelWidthInMhz channelBandwidth;
    /**
     * Tx空间流的最大数量
     */
    int maxNumberTxSpatialStreams;
    /**
     * Rx空间流的最大数量
     */
    int maxNumberRxSpatialStreams;
    /**
     * 遗留网络的详细网络模式
     */
    LegacyMode legacyMode;
};

/**
 * @brief 定义Wi-Fi的p2p网络信息 
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiP2pNetworkInfo {
    int id;
    unsigned char[] ssid;
    unsigned char[] bssid;
    unsigned char[] flags;
};

/**
 * @brief 定义Wi-Fi的p2p网络列表 
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiP2pNetworkList  {
    int infoNum;
    struct HdiP2pNetworkInfo[] infos;
};

/**
 * @brief 定义Wi-Fi的P2p设备信息 
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiP2pDeviceInfo {
    unsigned char[] srcAddress;
    unsigned char[] p2pDeviceAddress;
    unsigned char[] primaryDeviceType;
    unsigned char[] deviceName;
    int configMethods;
    int deviceCapabilities;
    int groupCapabilities;
    unsigned char[] wfdDeviceInfo;
    unsigned int wfdLength;
    unsigned char[] operSsid;
};

struct HdiP2pServiceInfo {
    int mode; /* 0/1, upnp/bonjour  */
    int version;
    unsigned char[] name;
    unsigned char[] query;
    unsigned char[] resp;
};

struct HdiP2pReqService {
    unsigned char[] bssid;
    unsigned char[] msg;
};

struct HdiP2pServDiscReqInfo {
    int freq;
    int dialogToken;
    int updateIndic;
    unsigned char[] mac;
    unsigned char[] tlvs;
};

struct HdiHid2dConnectInfo {
    unsigned char[] ssid;
    unsigned char[] bssid;
    unsigned char[] passphrase;
    int frequency;
};
struct HdiP2pConnectInfo {
    int persistent; /* |persistent=<network id>] */
    int mode; /* [join|auth] */
    int goIntent; /* [go_intent=<0..15>] */
    int provdisc; /* [provdisc] */
    unsigned char[] peerDevAddr;
    unsigned char[] pin; /* <pbc|pin|PIN#|p2ps> */
};

struct HdiP2pDeviceInfoParam {
    unsigned char[] srcAddress;
    unsigned char[] p2pDeviceAddress;
    unsigned char[] primaryDeviceType;
    unsigned char[] deviceName;
    int configMethods;
    int deviceCapabilities;
    int groupCapabilities;
    unsigned char[] wfdDeviceInfo;
    unsigned int wfdLength;
    unsigned char[] operSsid;
};

struct HdiP2pDeviceLostParam {
    unsigned char[] p2pDeviceAddress;
    int networkId;
};

struct HdiP2pGoNegotiationRequestParam {
    unsigned char[] srcAddress;
    int passwordId;
};

struct HdiP2pGoNegotiationCompletedParam {
    int status;
};

struct HdiP2pInvitationReceivedParam {
    int type; /* 0:Received, 1:Accepted */
    int persistentNetworkId;
    int operatingFrequency;
    unsigned char[] srcAddress;
    unsigned char[] goDeviceAddress;
    unsigned char[] bssid;
};

struct HdiP2pInvitationResultParam {
    int status;
    unsigned char[] bssid;
};

struct HdiP2pGroupStartedParam {
    int isGo;
    int isPersistent;
    int frequency;
    unsigned char[] groupIfName;
    unsigned char[] ssid;
    unsigned char[] psk;
    unsigned char[] passphrase;
    unsigned char[] goDeviceAddress;
};

struct HdiP2pGroupRemovedParam {
    int isGo;
    unsigned char[] groupIfName;
};

struct HdiP2pProvisionDiscoveryCompletedParam {
    int isRequest;
    int provDiscStatusCode;
    int configMethods;
    unsigned char[] p2pDeviceAddress;
    unsigned char[] generatedPin;
};

struct HdiP2pServDiscReqInfoParam {
    int freq;
    int dialogToken;
    int updateIndic;
    unsigned char[] mac;
    unsigned char[] tlvs;
};

struct HdiP2pServDiscRespParam {
    int updateIndicator;
    unsigned char[] srcAddress;
    unsigned char[] tlvs;
};

struct HdiP2pStaConnectStateParam {
    int state;
    unsigned char[] srcAddress;
    unsigned char[] p2pDeviceAddress;
};

struct HdiP2pIfaceCreatedParam {
    int isGo;
};

/**
 * @brief STA认证拒绝参数
 *
 * @since 4.1
 * @version 1.0
 */
struct HdiWpaAuthRejectParam {
    unsigned char[] bssid;
    unsigned short authType;
    unsigned short authTransaction;
    unsigned short statusCode;
};