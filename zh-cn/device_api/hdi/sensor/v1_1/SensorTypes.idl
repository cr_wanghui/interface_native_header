/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiSensor
 * @{
 *
 * @brief 传感器设备驱动对传感器服务提供通用的接口能力。
 *
 * 模块提供传感器服务对传感器驱动访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，
 * 以传感器ID区分访问不同类型传感器设备，实现获取传感器设备信息、订阅/取消订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度、量程等可选配置等。
 *
 * @version 1.1
 */

/**
 * @file SensorTypes.idl
 *
 * @brief 定义传感器模块所使用的传感器类型，传感器信息，传感器数据结构等数据类型。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @brief Sensor模块接口的包路径。
 *
 * @since 4.0
 * @version 1.1
 */
package ohos.hdi.sensor.v1_1;

/**
 * @brief 定义传感器的基本信息。
 *
 * 传感器的信息包括传感器名称、供应商、固件版本、硬件版本、传感器类型ID、传感器ID、最大测量范围、精度和功率。
 *
 * @since 2.2
 */
struct HdfSensorInformation {
    String sensorName; /**< 传感器名称。 */
    String vendorName; /**< 传感器供应商。 */
    String firmwareVersion; /**< 传感器固件版本。 */
    String hardwareVersion; /**< 传感器硬件版本。 */
    int sensorTypeId; /**< 传感器类型ID（在｛@link HdfSensorTypeTag｝中描述）。 */
    int sensorId;     /**< 传感器ID，由传感器驱动程序开发人员定义。 */
    float maxRange;   /**< 传感器的最大测量范围。 */
    float accuracy;   /**< 传感器精度。 */
    float power;      /**< 传感器功率。 */
    long minDelay; /**< 允许的最小采样周期（微秒） */
    long maxDelay; /**< 允许的最大采样周期（微秒） */
};

/**
 * @brief 定义传感器上报的数据。
 *
 * 上报的传感器数据包括传感器ID、传感器算法版本号、数据生成时间、传感器类型ID、
 * 数据选项（如测量范围和精度）、数据上报模式、数据地址、数据长度。
 *
 * @since 2.2
 */
struct HdfSensorEvents {
    int sensorId;            /**< 传感器ID。 */
    int version;             /**< 传感器算法版本号。 */
    long timestamp;           /**< 传感器数据生成时间。 */
    unsigned int option;     /**< 传感器数据选项，包括测量范围和精度。 */
    int mode;                /**< 传感器数据上报模式。 */
    unsigned char[] data;    /**< 传感器数据地址。 */
    unsigned int dataLen;    /**< 传感器数据长度。 */
};

/**
 * @brief Enumerates sensor types.
 *
 * @since 4.0
 */
enum HdfSensorTypeTag {
    HDF_SENSOR_TYPE_NONE                = 0,   /**< 空传感器类型，用于测试。 */
    HDF_SENSOR_TYPE_ACCELEROMETER       = 1,   /**< 加速度传感器。 */
    HDF_SENSOR_TYPE_GYROSCOPE           = 2,   /**< 陀螺仪传感器。 */
    HDF_SENSOR_TYPE_PHOTOPLETHYSMOGRAPH = 3,   /**< 心率传感器。 */
    HDF_SENSOR_TYPE_ELECTROCARDIOGRAPH  = 4,   /**< 心电传感器。 */
    HDF_SENSOR_TYPE_AMBIENT_LIGHT       = 5,   /**< 环境光传感器。 */
    HDF_SENSOR_TYPE_MAGNETIC_FIELD      = 6,   /**< 地磁传感器。 */
    HDF_SENSOR_TYPE_CAPACITIVE          = 7,   /**< 电容传感器。 */
    HDF_SENSOR_TYPE_BAROMETER           = 8,   /**< 气压计传感器。 */
    HDF_SENSOR_TYPE_TEMPERATURE         = 9,   /**< 温度传感器。 */
    HDF_SENSOR_TYPE_HALL                = 10,  /**< 霍尔传感器。 */
    HDF_SENSOR_TYPE_GESTURE             = 11,  /**< 手势传感器。 */
    HDF_SENSOR_TYPE_PROXIMITY           = 12,  /**< 接近光传感器。 */
    HDF_SENSOR_TYPE_HUMIDITY            = 13,  /**< 湿度传感器。 */
    HDF_SENSOR_TYPE_COLOR               = 14,  /**< 颜色传感器。 */
    HDF_SENSOR_TYPE_SAR                 = 15,  /**< SAR传感器。 */
    HDF_SENSOR_TYPE_AMBIENT_LIGHT1      = 16,  /**< 辅助环境光传感器。 */
    HDF_SENSOR_TYPE_MEDICAL_BEGIN       = 128, /**< 医疗传感器ID枚举值范围的开始。 */
    HDF_SENSOR_TYPE_MEDICAL_END         = 160, /**< 医疗传感器ID枚举值范围的结束。 */
    HDF_SENSOR_TYPE_PHYSICAL_MAX        = 255, /**< 物理传感器最大类型。 */
    HDF_SENSOR_TYPE_ORIENTATION         = 256, /**< 方向传感器。 */
    HDF_SENSOR_TYPE_GRAVITY             = 257, /**< 重力传感器。 */
    HDF_SENSOR_TYPE_LINEAR_ACCELERATION = 258, /**< 线性加速度传感器。 */
    HDF_SENSOR_TYPE_ROTATION_VECTOR     = 259, /**< 旋转矢量传感器。 */
    HDF_SENSOR_TYPE_AMBIENT_TEMPERATURE = 260, /**< 环境温度传感器。 */
    HDF_SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED = 261,   /**< 未校准磁场传感器。 */
    HDF_SENSOR_TYPE_GAME_ROTATION_VECTOR        = 262,   /**< 游戏旋转矢量传感器。 */
    HDF_SENSOR_TYPE_GYROSCOPE_UNCALIBRATED      = 263,   /**< 未校准陀螺仪传感器。 */
    HDF_SENSOR_TYPE_SIGNIFICANT_MOTION          = 264,   /**< 大幅度动作传感器。 */
    HDF_SENSOR_TYPE_PEDOMETER_DETECTION         = 265,   /**< 计步器检测传感器。 */
    HDF_SENSOR_TYPE_PEDOMETER                   = 266,   /**< 计步器传感器。 */
    HDF_SENSOR_TYPE_GEOMAGNETIC_ROTATION_VECTOR = 277,   /**< 地磁旋转矢量传感器。 */
    HDF_SENSOR_TYPE_HEART_RATE                  = 278,   /**< 心率传感器。 */
    HDF_SENSOR_TYPE_DEVICE_ORIENTATION          = 279,   /**< 设备方向传感器。 */
    HDF_SENSOR_TYPE_WEAR_DETECTION              = 280,   /**< 佩戴检测传感器。 */
    HDF_SENSOR_TYPE_ACCELEROMETER_UNCALIBRATED  = 281,   /**< 未校准加速度传感器。 */
    HDF_SENSOR_TYPE_MAX,                                 /**< 传感器类型最大个数标识。 */
};

/**
 * @brief 枚举传感器的硬件服务组。
 *
 * @since 2.2
 */
enum HdfSensorGroupType {
    HDF_TRADITIONAL_SENSOR_TYPE = 0, /**< 传统传感器类型，传感器ID枚举值范围不在128-160之间。 */
    HDF_MEDICAL_SENSOR_TYPE = 1,  /**< 医疗传感器类型，传感器ID枚举值范围在128-160之间。 */
    HDF_SENSOR_GROUP_TYPE_MAX,          /**< 最大传感器类型。 */
};
