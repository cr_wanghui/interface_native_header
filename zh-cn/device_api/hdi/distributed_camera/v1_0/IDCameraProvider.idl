/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Distributed Camera
 * @{
 *
 * @brief Distributed Camera模块接口定义。
 *
 * Distributed Camera模块包括对分布式相机设备的操作、流的操作和各种回调等，这部分接口与Camera一致。
 * 通过IDCameraProvider与Source SA通信交互，实现分布式功能。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IDCameraProvider.idl
 *
 * @brief 分布式相机SA服务和分布式相机HDF服务之间传输接口调用，并为上层提供硬件驱动接口。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Distributed Camera设备接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.distributed_camera.v1_0;

import ohos.hdi.distributed_camera.v1_0.DCameraTypes;
import ohos.hdi.distributed_camera.v1_0.IDCameraProviderCallback;

/**
 * @brief 定义Distributed Camera设备基本的操作。
 *
 * 启用分布式相机设备、设置流处理、更新控制参数、执行metadata等相关操作。
 */
interface IDCameraProvider {
    /**
     * @brief 启用分布式相机并设置回调。有关回调的详细信息可查看{@link IDCameraProviderCallback}。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param abilityInfo 分布式相机静态能力信息。
     * @param callbackObj 要设置的回调。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    EnableDCameraDevice([in] struct DHBase dhBase,[in] String abilityInfo,[in] IDCameraProviderCallback callbackObj);

    /**
     * @brief 禁用分布式相机。
     *
     * @param dhBase 分布式相机设备基础信息。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    DisableDCameraDevice([in] struct DHBase dhBase);

    /**
     * @brief 获取帧缓冲区。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param streamId 用于标识要获取的流。
     * @param buffer 帧缓冲区。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    AcquireBuffer([in] struct DHBase dhBase,[in] int streamId,[out] struct DCameraBuffer buffer);

    /**
     * @brief 当帧缓冲区已满时，通知分布式相机HDF服务。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param streamId 帧缓冲区要增加的流的ID。
     * @param buffer 输出帧缓冲区。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    ShutterBuffer([in] struct DHBase dhBase,[in] int streamId,[in] struct DCameraBuffer buffer);

    /**
     * @brief 上报分布式相机设备相关的数据。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param result 上报的数据。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    OnSettingsResult([in] struct DHBase dhBase,[in] struct DCameraSettings result);

    /**
     * @brief Source SA与分布式相机驱动的事件通知接口。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param event 详细事件内容。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    Notify([in] struct DHBase dhBase,[in] struct DCameraHDFEvent event);
}
