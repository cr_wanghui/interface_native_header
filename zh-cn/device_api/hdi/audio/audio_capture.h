/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义
 *
 * 音频接口涉及自定义类型、驱动加载接口、驱动适配器接口、音频播放（render）接口、音频录音（capture）接口等
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file audio_capture.h
 *
 * @brief Audio录音的接口定义文件
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef AUDIO_CAPTURE_H
#define AUDIO_CAPTURE_H

#include "audio_types.h"
#include "audio_control.h"
#include "audio_attribute.h"
#include "audio_scene.h"
#include "audio_volume.h"

struct AudioCapture {
    /**
     * @brief 音频控制能力接口，详情参考{@link AudioControl}
     */
    struct AudioControl control;

    /**
     * @brief 音频属性能力接口，详情参考{@link AudioAttribute}
     */
    struct AudioAttribute attr;

    /**
     * @brief 音频场景能力接口，详情参考{@link AudioScene}
     */
    struct AudioScene scene;

    /**
     * @brief 音频音量能力接口，详情参考{@link AudioVolume}
     */
    struct AudioVolume volume;

    /**
     * @brief 从音频驱动中录制（capture）一帧输入数据（录音，音频上行数据）
     *
     * @param capture 待操作的音频录音接口对象
     * @param frame 待存放输入数据的音频frame
     * @param requestBytes 待存放输入数据的音频frame大小（字节数）
     * @param replyBytes 实际读取到的音频数据长度（字节数），获取后保存到replyBytes中
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*CaptureFrame)(struct AudioCapture *capture, void *frame, uint64_t requestBytes, uint64_t *replyBytes);

    /**
     * @brief 获取音频输入帧数的上一次计数
     *
     * @param capture 待操作的音频录音接口对象
     * @param frames 获取的音频帧计数保存到frames中
     * @param time 获取的关联时间戳保存到time中
     * @return 成功返回值0，失败返回负值
     * @see CaptureFrame
     */
    int32_t (*GetCapturePosition)(struct AudioCapture *capture, uint64_t *frames, struct AudioTimeStamp *time);
};

#endif /* AUDIO_CAPTURE_H */
/** @} */
