/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiEffect
 * @{
 *
 * @brief Effect模块接口定义。
 *
 * 音效接口涉及数据类型、音效模型接口、音效控制器接口等。
 *
 * @since 4.0
 * @version 1.0
 */
 
 /**
 * @file EffectTypes.idl
 *
 * @brief Effect模块接口定义中使用的数据类型。
 *
 * Effect模块接口定义中使用的控制器参数、控制器描述符、音效输入输出buffer参数、控制命令等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief 音效接口的包路径。
 *
 * @since 4.0
 * @version 1.0
 */
package ohos.hdi.audio.effect.v1_0;

/**
 * @brief 定义effect加载的音效信息。
 *
 * @since 4.0
 * @version 1.0
 */
struct EffectInfo {
    String libName;         /**< 指定用于创建控制器的音效库名称 */
    String effectId;        /**< 音效ID */
    int ioDirection;        /**< 确定效果的方向 */
};

/**
 * @brief 定义效果控制器信息，包括其所属的库及其effectId。
 *
 * @since 4.0
 * @version 1.0
 */
struct ControllerId {
    String libName;         /**< 指定用于创建控制器的音效库名称 */
    String effectId;        /**< 音效ID */
};

/**
 * @brief 定义音效控制器描述
 *
 * @since 4.0
 * @version 1.0
 */
struct EffectControllerDescriptor {
    String effectId;                   /**< 音效控制器的ID */
    String effectName;                 /**< 音效控制器的名字 */
    String libName;                    /**< 音效库的名称*/
    String supplier;                   /**< 音效供应商的名字 */
};

/**
 * @brief 数据点类型标记，该类型正在按需使用。
 *
 * @since 4.0
 * @version 1.0
 */
enum AudioEffectBufferTag {
    EFFECT_BUFFER_VOID_TYPE       = 0,      /**< raw模式音频数据指向缓冲区的起点 */
    EFFECT_BUFFER_FLOAT_SIGNED_32 = 1 << 0, /**< 32bit浮点型音频数据指向缓冲区的起点 */
    EFFECT_BUFFER_SINGED_32       = 1 << 1, /**< 32bit整型音频数据指向缓冲区的起点 */
    EFFECT_BUFFER_SIGNED_16       = 1 << 2, /**< 16bit整型音频数据指向缓冲区的起点 */
    EFFECT_BUFFER_UNSIGNED_8      = 1 << 3, /**< 8bit无符号整型音频数据指向缓冲区的起点 */
};

/**
 * @brief 定义音效进程输入输出buffer。
 *
 * @since 4.0
 * @version 1.0
 */
struct AudioEffectBuffer {
    unsigned int frameCount;   /**< 帧缓冲区中的帧计数 */
    int datatag;               /**< 用于使用简化的数据点类型标记，祥见 {@link AudioEffectBufferTag} */
    byte[] rawData;            /**< 音频数据指向缓冲区的起点, 数据类型由datatag定义*/
};

/**
 * @brief 定义音效控制器命令索引。
 *
 * @since 4.0
 * @version 1.0
 */
enum EffectCommandTableIndex {
    AUDIO_EFFECT_COMMAND_INIT_CONTOLLER, /*< 初始化音效控制器 */
    AUDIO_EFFECT_COMMAND_SET_CONFIG,     /*< 设置配置参数 */
    AUDIO_EFFECT_COMMAND_GET_CONFIG,     /*< 获取配置参数 */
    AUDIO_EFFECT_COMMAND_RESET,          /*< 重启音效控制器 */
    AUDIO_EFFECT_COMMAND_ENABLE,         /*< 使能音效 */
    AUDIO_EFFECT_COMMAND_DISABLE,        /*< 禁用音效 */
    AUDIO_EFFECT_COMMAND_SET_PARAM,      /*< 设置参数 */
    AUDIO_EFFECT_COMMAND_GET_PARAM,      /*< 获取参数 */
};
/** @} */
