/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @addtogroup HdiA2dp
 * @{
 *
 * @brief HdiA2dp为A2DP服务提供统一接口。
 *
 * 主机可以通过该模块提供的接口创建音频通话，与音频子系统交换数据。
 *
 * @since 4.0
 */

/**
 * @file IBluetoothAudioCallback.idl
 *
 * @brief 声明回调函数，包含音频开始、暂停和结束操作。
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.bluetooth.a2dp.v1_0;

/**
 * @brief 声明用于音频渲染开启、暂停，和结束的回调函数。
 *
 * @since 4.0
 */
[callback] interface IBluetoothAudioCallback {
    /**
     * @brief 启动音频渲染的回调函数。
     *
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    StartRender();

    /**
     * @brief 暂停音频渲染的回调函数。
     *
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SuspendRender();

    /**
     * @brief 结束音频渲染的回调函数。
     *
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    StopRender();
}
