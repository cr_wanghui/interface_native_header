/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @addtogroup HdiAGnss
 * @{
 *
 * @brief AGNSS模块接口定义
 *
 * 上层可以使用该模块提供的接口设置AGNSS回调、AGNSS服务器地址、AGNSS参考信息、setId等。
 *
 * @since 3.2
 */

/*
 * @file AGnssTypes.idl
 *
 * @brief 定义AGNSS模块接口中使用到的数据结构。
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.agnss.v1_0;

/*
 * @brief 定义AGNSS参考信息类型。
 *
 * @since 3.2
 */
enum AGnssRefInfoType {
    /* 小区ID */
    ANSS_REF_INFO_TYPE_CELLID = 1,
    
    /* MAC地址 */
    ANSS_REF_INFO_TYPE_MAC = 2,
};

/*
 * @brief 定义AGNSS用户面的协议类型。
 *
 * @since 3.2
 */
enum AGnssUserPlaneProtocol {
    /* SUPL类型 */
    AGNSS_TYPE_SUPL         = 1,

    /* C2K类型 */
    AGNSS_TYPE_C2K          = 2,

    /* IMS类型 */
    AGNSS_TYPE_SUPL_IMS     = 3,

    /* EIMS类型 */
    AGNSS_TYPE_SUPL_EIMS    = 4
};

/*
 * @brief 定义数据链路的操作类型。
 *
 * @since 3.2
 */
enum DataLinkSetUpType {
    /* 请求建立数据业务连接。 */
    ESTABLISH_DATA_CONNECTION = 1,

    /* 请求释放数据业务连接。 */
    RELEASE_DATA_CONNECTION   = 2
};

/*
 * @brief 定义cell id类型。
 *
 * @since 3.2
 */
enum CellIdType {
    /* GSM小区 */
    CELLID_TYPE_GSM   = 1,

    /* UMTS小区 */
    CELLID_TYPE_UMTS  = 2,

    /* LTE小区 */
    CELLID_TYPE_LTE   = 3,

    /* NR小区 */
    CELLID_TYPE_NR    = 4,
};

/*
 * @brief 定义setid类型。
 *
 * @since 3.2
 */
enum SubscriberSetIdType {
    /* 未知类型 */
    SETID_TYPE_NONE    = 0,

    /* IMSI类型 */
    SETID_TYPE_IMSI    = 1,

    /* MSISDM类型 */
    SETID_TYPE_MSISDM  = 2
};

/*
 * @brief 定义AGNSS参考信息中cellId的结构体。
 *
 * @since 3.2
 */
struct AGnssRefCellId {
    /* 小区ID类型 */
    enum CellIdType type;

    /* 移动设备国家码 */
    unsigned short mcc;

    /* 移动设备网络码 */
    unsigned short mnc;

    /* 位置区域码 */
    unsigned short lac;

    /* 小区ID */
    unsigned int cid;

    /* 跟踪区域码 */
    unsigned short tac;

    /* 物理小区ID */
    unsigned short pcid;

    /* NR的小区ID */
    unsigned int nci;
};

/*
 * @brief 定义AGNSS服务器信息结构体。
 *
 * @since 3.2
 */
struct AGnssServerInfo {
    /* AGNSS用户面协议类型 */
    enum AGnssUserPlaneProtocol type;

    /* AGNSS服务器地址 */
    String server;

    /* AGNSS服务器端口 */
    int port;
};

/*
 * @brief 定义setId结构体。
 *
 * @since 3.2
 */
struct SubscriberSetId {
    /* setId类型 */
    enum SubscriberSetIdType type;

    /* setId字符串 */
    String id;
};

/*
 * @brief 定义AGNSS参考信息中MAC的结构体。
 *
 * @since 3.2
 */
struct AGnssRefMac {
    /* MAC地址 */
    unsigned char[] mac;
};

/*
 * @brief 定义AGNSS参考信息结构体。
 *
 * @since 3.2
 */
struct AGnssRefInfo {
    /* 参考信息类型 */
    enum AGnssRefInfoType type;

    /* 小区ID */
    struct AGnssRefCellId   cellId;

    /* MAC地址 */
    struct AGnssRefMac      mac;
};

/*
 * @brief 定义操作数据业务连接请求的结构体。
 *
 * @since 3.2
 */
struct AGnssDataLinkRequest {
    /* AGNSS用户面协议类型 */
    enum AGnssUserPlaneProtocol agnssType;

    /* 数据业务操作类型 */
    enum DataLinkSetUpType setUpType;
};
/** @} */