/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceCellbatching
 * @{
 *
 * @brief 为低功耗围栏服务提供基站轨迹数据记录的API。
 *
 * 本模块能够控制设备对接收的基站数据进行缓存和上报。
 * 应用场景：根据设备接收的基站轨迹数据，判断用户的大致活动区域，进而进行一些服务提醒。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file CellbatchingTypes.idl
 *
 * @brief 定义基站轨迹数据记录模块使用的数据类型。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief 基站轨迹数据记录模块接口的包路径。
 *
 * @since 4.0
 */
package ohos.hdi.location.lpfence.cellbatching.v1_0;

/**
 * @brief 枚举对基站轨迹数据记录的开关操作。
 *
 * @since 4.0
 */
enum CellbatchingSwitch {
    /** 关闭基站轨迹数据记录功能。 */
    SWITCH_OFF = 0,
    /** 打开基站轨迹数据记录功能。 */
    SWITCH_ON = 1,
};

/**
 * @brief 定义设置基站轨迹数据上报功能的数据结构。
 *
 * @since 4.0
 */
struct CellbatchingRequest {
    /** 基站轨迹数据记录功能开关，详见{@Link CellbatchingSwitch} */
    int status;
    /** 设置modem接收基站数据的时间间隔，单位为秒，最小间隔30秒，最大间隔240秒 */
    int interval;
};

/**
 * @brief 定义上报的基站轨迹数据的数据结构。
 *
 * @since 4.0
 */
struct CellTrajectoryData {
    /** 时间戳的低32位 */
    unsigned int timeStampLow;
    /** 时间戳的高32位 */
    unsigned int timeStampHigh;
    /** 基站号 */
    unsigned lone cid;
    /** 小区号 */
    unsigned int lac;
    /** 信号接收强度 */
    unsigned short rssi;
    /** 移动国家码 */
    unsigned short mcc;
    /** 移动网络码 */
    unsigned short mnc;
};
/** @} */