/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceWififence
 * @{
 *
 * @brief 为低功耗围栏服务提供Wi-Fi围栏的API
 *
 * 本模块接口提供添加Wi-Fi围栏，删除Wi-Fi围栏，获取Wi-Fi围栏状态，获取Wi-Fi围栏使用信息的功能。
 * 应用场景：一般用于判断设备是否在室内特定位置，如居所内或商场的某个店铺内。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IWififenceCallback.idl
 *
 * @brief 定义Wi-Fi围栏模块回调接口。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief Wi-Fi围栏模块接口的包路径。
 *
 * @since 4.0
 */
package ohos.hdi.location.lpfence.wififence.v1_0;

/**
 * @brief 导入Wi-Fi围栏模块的数据类型。
 *
 * @since 4.0
 */
import ohos.hdi.location.lpfence.wififence.v1_0.WififenceTypes;

/**
 * @brief 定义Wi-Fi围栏模块的回调函数
 *
 * 用户在开启Wi-Fi围栏功能前，需要先注册该回调函数。当Wi-Fi围栏状态发生变化时，会通过回调函数进行上报。
 * 详情可参考{@link ICellfenceInterface}。 
 *
 * @since 4.0
 */
[callback] interface IWififenceCallback {
    /**
     * @brief 定义Wi-Fi围栏状态变化的回调函数。
     *
     * 设备与Wi-Fi围栏的状态关系发生变化时，会通过该回调函数进行上报。
     *
     * @param wififenceId Wi-Fi围栏ID号。
     * @param transition Wi-Fi围栏变化的状态。详见{@link WififenceTransition}定义。
     * @param timeStamp 时间戳。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnWififenceChanged([in] int wififenceId,
                       [in] int transition,
                       [in] long timeStamp);

    /**
     * @brief 定义Wi-Fi围栏使用信息的回调函数。
     *
     * 获取Wi-Fi围栏使用信息时，会通过该回调函数进行上报。
     *
     * @param size 基站围栏使用信息。详见{@link WififenceSize}定义。
     *
     * @return 如果回调函数上报数据成功，则返回0。
     * @return 如果回调函数上报数据失败，则返回负值。
     *
     * @since 4.0
     */
    OnGetWififenceSizeCb([in] struct WififenceSize size);

    /**
     * @brief 定义低功耗围栏服务复位事件通知的回调函数。
     *
     * 低功耗围栏服务发生复位时会通过该回调函数进行事件上报。
     *
     * @return 如果回调函数调用成功，则返回0。
     * @return 如果回调函数调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnWififenceReset();
}
/** @} */