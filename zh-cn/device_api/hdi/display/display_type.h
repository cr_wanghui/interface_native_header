/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给图形系统使用的驱动接口，包括图层管理、设备控制、图形硬件加速、显示内存管理和回调接口等。
 *
 * @since 1.0
 * @version 2.0
 */

/**
 * @file display_type.h
 *
 * @brief 显示类型定义，定义显示驱动接口所使用的数据类型。
 *
 * @since 1.0
 * @version 2.0
 */

#ifndef DISPLAY_TYPE_H
#define DISPLAY_TYPE_H
#include <fcntl.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include "buffer_handle.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 返回值类型定义。
 *
 */
typedef enum {
    DISPLAY_SUCCESS = 0,           /**< 成功 */
    DISPLAY_FAILURE = -1,          /**< 失败 */
    DISPLAY_FD_ERR = -2,           /**< fd错误 */
    DISPLAY_PARAM_ERR = -3,        /**< 参数错误 */
    DISPLAY_NULL_PTR = -4,         /**< 空指针 */
    DISPLAY_NOT_SUPPORT = -5,      /**< 不支持的特性 */
    DISPLAY_NOMEM = -6,            /**< 内存不足 */
    DISPLAY_SYS_BUSY = -7,         /**< 系统繁忙 */
    DISPLAY_NOT_PERM = -8          /**< 操作不允许 */
} DispErrCode;

/**
 * @brief 图层类型定义。
 *
 */
typedef enum {
    LAYER_TYPE_GRAPHIC,         /**< 图形层 */
    LAYER_TYPE_OVERLAY,         /**< 视频层 */
    LAYER_TYPE_SIDEBAND,       /**< 媒体播放 */
    LAYER_TYPE_CURSOR,          /**< 光标层 */
    LAYER_TYPE_BUTT             /**< 空图层 */
} LayerType;

/**
 * @brief 定义缓冲区使用。
 *
 */
enum {
    HBM_USE_CPU_READ = (1 << 0),        /**< CPU 读缓冲 */
    HBM_USE_CPU_WRITE = (1 << 1),       /**< CPU 写内存 */
    HBM_USE_MEM_MMZ = (1 << 2),         /**< MMZ */
    HBM_USE_MEM_DMA = (1 << 3),         /**< DMA缓冲区 */
    HBM_USE_MEM_SHARE = (1 << 4),       /**< 共享内存缓冲区 */
    HBM_USE_MEM_MMZ_CACHE = (1 << 5),   /**< MMZ缓存 */
    HBM_USE_MEM_FB = (1 << 6),          /**< 帧缓存 */
    HBM_USE_ASSIGN_SIZE = (1 << 7),     /**< 内存分配 */
};

/**
 * @brief 像素格式类型定义。
 *
 */
typedef enum {
    PIXEL_FMT_CLUT8 = 0,                 /**< CLUT8 格式 */
    PIXEL_FMT_CLUT1,                     /**< CLUT1 格式 */
    PIXEL_FMT_CLUT4,                     /**< CLUT4 格式 */
    PIXEL_FMT_RGB_565,                   /**< RGB565 格式 */
    PIXEL_FMT_RGBA_5658,                 /**< RGBA5658 格式 */
    PIXEL_FMT_RGBX_4444,                 /**< RGBX4444 格式 */
    PIXEL_FMT_RGBA_4444,                 /**< RGBA4444 格式 */
    PIXEL_FMT_RGB_444,                   /**< RGB444 格式 */
    PIXEL_FMT_RGBX_5551,                 /**< RGBX5551 格式 */
    PIXEL_FMT_RGBA_5551,                 /**< RGBA5551 格式 */
    PIXEL_FMT_RGB_555,                   /**< RGB555 格式 */
    PIXEL_FMT_RGBX_8888,                 /**< RGBX8888 格式 */
    PIXEL_FMT_RGBA_8888,                 /**< RGBA8888 格式 */
    PIXEL_FMT_RGB_888,                   /**< RGB888 格式 */
    PIXEL_FMT_BGR_565,                   /**< BGR565 格式 */
    PIXEL_FMT_BGRX_4444,                 /**< BGRX4444 格式 */
    PIXEL_FMT_BGRA_4444,                 /**< BGRA4444 格式 */
    PIXEL_FMT_BGRX_5551,                 /**< BGRX5551 格式 */
    PIXEL_FMT_BGRA_5551,                 /**< BGRA5551 格式 */
    PIXEL_FMT_BGRX_8888,                 /**< BGRX8888 格式 */
    PIXEL_FMT_BGRA_8888,                 /**< BGRA8888 格式 */
    PIXEL_FMT_YUV_422_I,                 /**< YUV422 交错格式 */
    PIXEL_FMT_YCBCR_422_SP,              /**< YCBCR422 半平面格式 */
    PIXEL_FMT_YCRCB_422_SP,              /**< YCRCB422 半平面格式 */
    PIXEL_FMT_YCBCR_420_SP,              /**< YCBCR420 半平面格式 */
    PIXEL_FMT_YCRCB_420_SP,              /**< YCRCB420 半平面格式 */
    PIXEL_FMT_YCBCR_422_P,               /**< YCBCR422 平面格式 */
    PIXEL_FMT_YCRCB_422_P,               /**< YCRCB422 平面格式 */
    PIXEL_FMT_YCBCR_420_P,               /**< YCBCR420 平面格式 */
    PIXEL_FMT_YCRCB_420_P,               /**< YCRCB420 平面格式 */
    PIXEL_FMT_YUYV_422_PKG,              /**< YUYV422 打包格式 */
    PIXEL_FMT_UYVY_422_PKG,              /**< UYVY422 打包格式t */
    PIXEL_FMT_YVYU_422_PKG,              /**< YVYU422 打包格式 */
    PIXEL_FMT_VYUY_422_PKG,              /**< VYUY422 打包格式*/
    PIXEL_FMT_VENDER_MASK = 0X7FFF0000,  /**< vendor mask 格式 */
    PIXEL_FMT_BUTT = 0X7FFFFFFF          /**< Invalid 像素格式 */
} PixelFormat;

/**
 * @brief 图层变换类型定义。
 *
 */
typedef enum {
    ROTATE_NONE = 0,        /**< 不旋转 */
    ROTATE_90,              /**< 旋转90度 */
    ROTATE_180,             /**< 旋转180度 */
    ROTATE_270,             /**< 旋转270度 */
    ROTATE_BUTT             /**< 无效操作 */
} TransformType;

/**
 * @brief 支持的图像混合类型。
 *
 * 系统在硬件加速期间基于指定的混合类型合成图像。
 *
 */
typedef enum {
    BLEND_NONE = 0,         /**< No 混合操作 */
    BLEND_CLEAR,            /**< CLEAR 混合操作 */
    BLEND_SRC,              /**< SRC 混合操作 */
    BLEND_SRCOVER,          /**< SRC_OVER 混合操作 */
    BLEND_DSTOVER,          /**< DST_OVER 混合操作 */
    BLEND_SRCIN,            /**< SRC_IN 混合操作 */
    BLEND_DSTIN,            /**< DST_IN 混合操作 */
    BLEND_SRCOUT,           /**< SRC_OUT 混合操作 */
    BLEND_DSTOUT,           /**< DST_OUT 混合操作 */
    BLEND_SRCATOP,          /**< SRC_ATOP 混合操作 */
    BLEND_DSTATOP,          /**< DST_ATOP 混合操作 */
    BLEND_ADD,              /**< ADD 混合操作 */
    BLEND_XOR,              /**< XOR 混合操作 */
    BLEND_DST,              /**< DST 混合操作 */
    BLEND_AKS,              /**< AKS 混合操作 */
    BLEND_AKD,              /**< AKD 混合操作 */
    BLEND_BUTT              /**< 空操作 */
} BlendType;

/**
 * @brief 硬件加速支持的ROP操作类型。
 *
 * 硬件加速支持的ROP操作类型，在将前景位图的RGB颜色分量和Alpha分量值与背景位图的RGB颜色
 * 分量值和Alpha分量值进行按位的布尔运算（包括按位与，按位或等），将结果输出。
 *
 */
typedef enum {
    ROP_BLACK = 0,          /**< 黑色 */
    ROP_NOTMERGEPEN,        /**< ~(S2+S1) */
    ROP_MASKNOTPEN,         /**< ~S2&S1 */
    ROP_NOTCOPYPEN,         /**< ~S2 */
    ROP_MASKPENNOT,         /**< S2&~S1 */
    ROP_NOT,                /**< ~S1 */
    ROP_XORPEN,             /**< S2^S1 */
    ROP_NOTMASKPEN,         /**< ~(S2&S1) */
    ROP_MASKPEN,            /**< S2&S1 */
    ROP_NOTXORPEN,          /**< ~(S2^S1) */
    ROP_NOP,                /**< S1 */
    ROP_MERGENOTPEN,        /**< ~S2+S1 */
    ROP_COPYPE,             /**< S2 */
    ROP_MERGEPENNOT,        /**< S2+~S1 */
    ROP_MERGEPEN,           /**< S2+S1 */
    ROP_WHITE,              /**< 白色 */
    ROP_BUTT                /**< 无效值 */
} RopType;

/**
 * @brief Color key操作类型定义，即硬件加速支持的Color key操作类型。
 *
 */
typedef enum {
    CKEY_NONE = 0,      /**< 不使用colorkey */
    CKEY_SRC,           /**< 使用源colorkey */
    CKEY_DST,           /**< 使用目标colorkey */
    CKEY_BUTT           /**< 空操作 */
} ColorKey;

/**
 * @brief 硬件加速支持的镜像操作类型定义
 *
 */
typedef enum {
    MIRROR_NONE = 0,      /**< 不使用镜像 */
    MIRROR_LR,            /**< 左右镜像 */
    MIRROR_TB,            /**< 上下镜像 */
    MIRROR_BUTT           /**< 空操作 */
} MirrorType;

/**
 * @brief 热插拔连接类型定义
 *
 */
typedef enum {
    CON_INVALID = 0,            /**< 无效类型 */
    CONNECTED,                  /**< 已连接 */
    DISCONNECTED                /**< 断开连接 */
} Connection;

/**
 * @brief 定义显示信息结构体
 *
 */
typedef struct {
    uint32_t width;              /**< 显示屏宽度 */
    uint32_t height;             /**< 显示屏高度 */
    int32_t rotAngle;            /**< 显示屏旋转角度 */
} DisplayInfo;

/**
 * @brief 定义图层信息结构体
 *
 * 在创建图层时，需要将LayerInfo传递给创建图层接口，创建图层接口根据图层信息创建相应图层。
 *
 */
typedef struct {
    int32_t width;              /**< 图层宽度 */
    int32_t height;             /**< 图层高度 */
    LayerType type;             /**< 图层类型，包括图形层、视频层和媒体播放模式 */
    int32_t bpp;                /**< 每像素所占bit数 */
    PixelFormat pixFormat;      /**< 图层像素格式 */
} LayerInfo;

/**
 * @brief 定义图层Alpha信息的结构体
 *
 */
typedef struct {
    bool enGlobalAlpha;   /**< 全局alpha使能标志 */
    bool enPixelAlpha;    /**< 像素alpha使能标志 */
    uint8_t alpha0;       /**< alpha0值，取值范围：[0, 255] */
    uint8_t alpha1;       /**< alpha1值，取值范围：[0, 255] */
    uint8_t gAlpha;       /**< 全局alpha值，取值范围：[0, 255] */
} LayerAlpha;


/**
 * @brief 定义一层的缓冲区数据，包括虚拟和物理内存地址。
 *
 */
typedef struct {
    uint64_t phyAddr;     /**< 物理内存地址 */
    void *virAddr;        /**< 虚拟内存地址 */
} BufferData;

/**
 * @brief 图层Buffer，用于存放图层数据。
 *
 */
typedef struct {
    int32_t fenceId;          /**< buffer 的fence号r */
    int32_t width;            /**< buffer宽度 */
    int32_t height;           /**< buffer高度 */
    int32_t pitch;            /**< 一行数据所占字节数 */
    PixelFormat pixFormat;    /**< buffer像素格式r */
    BufferData data;          /**< 图层buffer数据 */
    BufferHandle* hdl;        /**< 图层buffer句柄 */
} LayerBuffer;

/**
 * @brief 定义矩形信息
 *
 */
typedef struct {
    int32_t x;      /**< 矩形框起始x坐标 */
    int32_t y;      /**< 矩形框起始y坐标 */
    int32_t w;      /**< 矩形框宽度 */
    int32_t h;      /**< 矩形框高度 */
} IRect;

/**
 * @brief 用于存放窗口相关信息的结构体定义，提供给硬件加速使用，例如图像合成，位图搬移等操作。
 */
typedef struct {
    uint64_t phyAddr;         /**< 图像首地址 */
    int32_t height;           /**< 图像高度 */
    int32_t width;            /**< 图像宽度 */
    int32_t stride;           /**< 图像跨度 */
    PixelFormat enColorFmt;   /**< 图像格式 */
    bool bYCbCrClut;          /**< CLUT表是否位于 YCbCr 空间 */
    bool bAlphaMax255;        /**< 图像alpha最大值为255还是128 */
    bool bAlphaExt1555;       /**< 是否使能1555的Alpha扩展 */
    uint8_t alpha0;           /**< Alpha0值，取值范围：[0,255] */
    uint8_t alpha1;           /**< Alpha1值，取值范围：[0,255] */
    uint64_t cbcrPhyAddr;     /**< CbCr分量地址 */
    int32_t cbcrStride;       /**< CbCr分量跨度 */
    uint64_t clutPhyAddr;     /**< Clut表首地址，用作颜色扩展或颜色校正 */
} ISurface;

/**
 * @brief 线条描述结构体定义，用于硬件加速绘制直线。
 *
 */
typedef struct {
    int32_t x0;                 /**< 线条起点的x坐标 */
    int32_t y0;                 /**< 线条起点的y坐标 */
    int32_t x1;                 /**< 线条终点的x坐标 */
    int32_t y1;                 /**< 线条终点的y坐标 */
    uint32_t color;             /**< 线条颜色 */
} ILine;

/**
 * @brief 圆形描述结构体定义，用于硬件加速绘制圆形。
 *
 */
typedef struct {
    int32_t x;                  /**< 圆心x坐标 */
    int32_t y;                  /**< 圆心y坐标r */
    int32_t r;                  /**< 圆的半径 */
    uint32_t color;             /**< 圆的颜色 */
} ICircle;

/**
 * @brief 矩形描述结构体定义，用于硬件加速绘制矩形，
 *
 */
typedef struct {
    IRect rect;                 /**< 矩形区域 */
    uint32_t color;             /**< 矩形颜色 */
} Rectangle;

/**
 * @brief 图像硬件加速相关的操作选项结构体定义，用于图像硬件加速时的操作选项。
 *
 */
typedef struct {
    bool enGlobalAlpha;         /**< 全局alpha使能位 */
    uint32_t globalAlpha;       /**< 全局alpha的值 */
    bool enPixelAlpha;          /**< 像素alpha使能位 */
    BlendType blendType;        /**< 混合方式 */
    ColorKey colorKeyFrom;      /**< 色键模式 */
    bool enableRop;             /**< Rop功能使能位 */
    RopType colorRopType;       /**< 颜色的Rop类型 */
    RopType alphaRopType;       /**< Alpha的Rop类型 */
    bool enableScale;           /**< 缩放功能使能位 */
    TransformType rotateType;   /**< 旋转类型 */
    MirrorType mirrorType;      /**< 镜像类型 */
} GfxOpt;

/** 属性名称长度 */
#define PROPERTY_NAME_LEN  50

/**
 * @brief 定义包含名称、属性ID和值的属性对象。
 *
 */
typedef struct {
    char name[PROPERTY_NAME_LEN]; /**< 属性名称 */
    uint32_t propId;     /**< 属性ID */
    uint64_t value;      /**< 属性值 */
} PropertyObject;

/**
 * @brief 枚举接口类型。
 *
 */
typedef enum {
    DISP_INTF_HDMI = 0,       /**< HDMI 接口 */
    DISP_INTF_LCD,            /**< LCD 接口 */
    DISP_INTF_BT1120,         /**< BT1120 接口 */
    DISP_INTF_BT656,          /**< BT656 接口 */
    DISP_INTF_YPBPR,          /**< YPBPR 接口 */
    DISP_INTF_RGB,            /**< RGB 接口 */
    DISP_INTF_CVBS,           /**< CVBS 接口 */
    DISP_INTF_SVIDEO,         /**< SVIDEO 接口 */
    DISP_INTF_VGA,            /**< VGA 接口 */
    DISP_INTF_MIPI,           /**< MIPI 接口 */
    DISP_INTF_PANEL,          /**< PANEL 接口 */
    DISP_INTF_BUTT,           /**< BUTT 接口, 一个不可用类型, 用于默认初始化 */
} InterfaceType;

/**
 * @brief 定义输出性能。
 */
typedef struct {
    char name[PROPERTY_NAME_LEN];       /**< 显示设备名称 */
    InterfaceType type;                 /**< 显示屏接口类型 */
    uint32_t phyWidth;                  /**< 物理宽度 */
    uint32_t phyHeight;                 /**< 物理高度 */
    uint32_t supportLayers;             /**< 支持的图层数 */
    uint32_t virtualDispCount;          /**< 支持的虚拟屏数 */
    bool supportWriteBack;              /**< 是否支持回写 */
    uint32_t propertyCount;             /**< 属性数组大小 */
    PropertyObject* props;              /**< 属性数组*/
} DisplayCapability;

/**
 * @brief 定义输出模式信息。
 */
typedef struct {
    int32_t width;          /**< 像素宽度 */
    int32_t height;         /**< 像素高度 */
    uint32_t freshRate;     /**< 刷新速率 */
    int32_t id;             /**< 模式ID */
} DisplayModeInfo;

/**
 * @brief 定义关于要分配的内存的信息。
 *
 */
typedef struct {
    uint32_t width;               /**< 申请内存宽度 */
    uint32_t height;              /**< 申请内存高度 */
    uint64_t usage;               /**< 申请内存的使用场景 */
    PixelFormat format;           /**< 申请内存格式 */
    uint32_t expectedSize;        /**< 申请内存大小 */
} AllocInfo;
/**
 * @brief 枚举显示状态
 */

typedef enum {
    POWER_STATUS_ON,              /**< 上电模式 */
    POWER_STATUS_STANDBY,         /**< 待机模式 */
    POWER_STATUS_SUSPEND,         /**< 休眠模式 */
    POWER_STATUS_OFF,             /**< 下电模式 */
    POWER_STATUS_BUTT             /**< 默认模式 */
} DispPowerStatus;

/**
 * @brief 枚举特殊层的组合类型。
 */
typedef enum {
    COMPOSITION_CLIENT,       /**< Client 合成类型,使用CPU或者GPU合成 */
    COMPOSITION_DEVICE,       /**< Device 合成类型，使用Device合成 */
    COMPOSITION_CURSOR,       /**< Cursor合成类型，用于光标合成 */
    COMPOSITION_VIDEO,        /**< Video合成类型，用于视频层合成 */
    COMPOSITION_DEVICE_CLEAR, /**< Device clear 合成类型, device会清除目标区域 */
    COMPOSITION_CLIENT_CLEAR, /**< Client clear 合成类型, service会清除目标区域 */
    COMPOSITION_TUNNEL,       /**< Tunnel 合成类型, 用于tunnel. */
    COMPOSITION_BUTT,         /**< 合成类型, 一个不可用类型, 用于默认初始化 */
} CompositionType;

/**
 * @brief 色域类型枚举值
 *
 */
typedef enum {
    COLOR_GAMUT_INVALID = -1,            /**< 无效值 */
    COLOR_GAMUT_NATIVE = 0,              /**< 默认值 */
    COLOR_GAMUT_STANDARD_BT601 = 1,      /**< Standard BT601类型 */
    COLOR_GAMUT_STANDARD_BT709 = 2,      /**< Standard BT709类型 */
    COLOR_GAMUT_DCI_P3 = 3,              /**< DCI P3类型 */
    COLOR_GAMUT_SRGB = 4,                /**< SRGB类型 */
    COLOR_GAMUT_ADOBE_RGB = 5,           /**< Adobe RGB类型 */
    COLOR_GAMUT_DISPLAY_P3 = 6,          /**< display P3类型 */
    COLOR_GAMUT_BT2020 = 7,              /**< BT2020类型 */
    COLOR_GAMUT_BT2100_PQ = 8,           /**< BT2100 PQ类型 */
    COLOR_GAMUT_BT2100_HLG = 9,          /**< BT2100 HLG类型 */
    COLOR_GAMUT_DISPLAY_BT2020 = 10,     /**< Display BT2020类型 */
} ColorGamut;

/**
 * @brief 枚举色域的映射类型
 *
 */
typedef enum {
    GAMUT_MAP_CONSTANT = 0,         /**< 不变 */
    GAMUT_MAP_EXPANSION = 1,        /**< 映射增强 */
    GAMUT_MAP_HDR_CONSTANT = 2,     /**< 不变，用于HDR */
    GAMUT_MAP_HDR_EXPANSION = 3,    /**< 映射增强, 用于HDR */
} GamutMap;

/**
 * @brief 枚举颜色空间的类型
 *
 */

typedef enum {
    /** 未知的 */
    COLOR_DATA_SPACE_UNKNOWN = 0,
    /** BT601色域 */
    GAMUT_BT601 = 0x00000001,
    /** BT709色域 */
    GAMUT_BT709 = 0x00000002,
    /** DCI_P3色域 */
    GAMUT_DCI_P3 = 0x00000003,
    /** SRGB色域 */
    GAMUT_SRGB = 0x00000004,
    /** ADOBE_RGB色域 */
    GAMUT_ADOBE_RGB = 0x00000005,
    /** DISPLAY_P3色域 */
    GAMUT_DISPLAY_P3 = 0x00000006,
    /** BT2020色域 */
    GAMUT_BT2020 = 0x00000007,
    /** BT2100_PQ色域 */
    GAMUT_BT2100_PQ = 0x00000008,
    /** BT2100_HLG色域 */
    GAMUT_BT2100_HLG = 0x00000009,
    /** DISPLAY_BT2020色域 */
    GAMUT_DISPLAY_BT2020 = 0x0000000a,
    /** UNSPECIFIED转换函数 */
    TRANSFORM_FUNC_UNSPECIFIED = 0x00000100,
    /** LINEAR转换函数 */
    TRANSFORM_FUNC_LINEAR = 0x00000200,
    /** SRGB转换函数 */
    TRANSFORM_FUNC_SRGB = 0x00000300,
    /** SMPTE_170M转换函数 */
    TRANSFORM_FUNC_SMPTE_170M = 0x00000400,
    /** GM2_2转换函数 */
    TRANSFORM_FUNC_GM2_2 = 0x00000500,
    /** GM2_6转换函数 */
    TRANSFORM_FUNC_GM2_6 = 0x00000600,
    /** GM2_8转换函数 */
    TRANSFORM_FUNC_GM2_8 = 0x00000700,
    /** ST2084转换函数 */
    TRANSFORM_FUNC_ST2084 = 0x00000800,
    /** HLG转换函数 */
    TRANSFORM_FUNC_HLG = 0x00000900,
    /** UNSPECIFIED精度 */
    PRECISION_UNSPECIFIED = 0x00010000,
    /** FULL精度 */
    PRECISION_FULL = 0x00020000,
    /** LIMITED精度 */
    PRESION_LIMITED = 0x00030000,
    /** EXTENDED精度 */
    PRESION_EXTENDED = 0x00040000,
    /** BT601色域 | SMPTE_170M转换函数 | FULL精度 */
    BT601_SMPTE170M_FULL = GAMUT_BT601 | TRANSFORM_FUNC_SMPTE_170M | PRECISION_FULL,
    /** BT601色域 | SMPTE_170M转换函数 | LIMITED精度 */
    BT601_SMPTE170M_LIMITED = GAMUT_BT601 | TRANSFORM_FUNC_SMPTE_170M | PRESION_LIMITED,
    /** BT709色域 | LINEAR转换函数 | FULL精度 */
    BT709_LINEAR_FULL = GAMUT_BT709 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** BT709色域 | LINEAR转换函数 | EXTENDED精度 */
    BT709_LINEAR_EXTENDED = GAMUT_BT709 | TRANSFORM_FUNC_LINEAR | PRESION_EXTENDED,
    /** BT709色域 | SRGB转换函数 | FULL精度 */
    BT709_SRGB_FULL = GAMUT_BT709 | TRANSFORM_FUNC_SRGB | PRECISION_FULL,
    /** BT709色域 | SRGB转换函数 | EXTENDED精度 */
    BT709_SRGB_EXTENDED = GAMUT_BT709 | TRANSFORM_FUNC_SRGB | PRESION_EXTENDED,
    /** BT709色域 | SMPTE_170M转换函数 | LIMITED精度 */
    BT709_SMPTE170M_LIMITED = GAMUT_BT709 | TRANSFORM_FUNC_SMPTE_170M | PRESION_LIMITED,
    /** DCI_P3色域 | LINEAR转换函数 | FULL精度 */
    DCI_P3_LINEAR_FULL = GAMUT_DCI_P3 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** DCI_P3色域 | GM2_6转换函数 | FULL精度 */
    DCI_P3_GAMMA26_FULL = GAMUT_DCI_P3 | TRANSFORM_FUNC_GM2_6 | PRECISION_FULL,
    /** DISPLAY_P3色域 | LINEAR转换函数 | FULL精度 */
    DISPLAY_P3_LINEAR_FULL = GAMUT_DISPLAY_P3 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** DCI_P3色域 | SRGB转换函数 | FULL精度 */
    DCI_P3_SRGB_FULL = GAMUT_DCI_P3 | TRANSFORM_FUNC_SRGB | PRECISION_FULL,
    /** ADOBE_RGB色域 | GM2_2转换函数 | FULL精度 */
    ADOBE_RGB_GAMMA22_FULL = GAMUT_ADOBE_RGB | TRANSFORM_FUNC_GM2_2 | PRECISION_FULL,
    /** BT2020色域 | LINEAR转换函数 | FULL精度 */
    BT2020_LINEAR_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_LINEAR | PRECISION_FULL,
    /** BT2020色域 | SRGB转换函数 | FULL精度 */
    BT2020_SRGB_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_SRGB | PRECISION_FULL,
    /** BT2020色域 | SMPTE_170M转换函数 | FULL精度 */
    BT2020_SMPTE170M_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_SMPTE_170M | PRECISION_FULL,
    /** BT2020色域 | ST2084转换函数 | FULL精度 */
    BT2020_ST2084_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_ST2084 | PRECISION_FULL,
    /** BT2020色域 | HLG转换函数 | FULL精度 */
    BT2020_HLG_FULL = GAMUT_BT2020 | TRANSFORM_FUNC_HLG | PRECISION_FULL,
    /** BT2020色域 | ST2084转换函数 | LIMITED精度 */
    BT2020_ST2084_LIMITED = GAMUT_BT2020 | TRANSFORM_FUNC_ST2084 | PRESION_LIMITED,
} ColorDataSpace;

/**
 * @brief 枚举HDR格式
 *
 */
typedef enum {
    NOT_SUPPORT_HDR = 0,         /**< 不支持HDR */
    DOLBY_VISION = 1,            /**< Dolby Vision格式 */
    HDR10 = 2,                   /**< HDR10格式 */
    HLG = 3,                     /**< HLG格式 */
    HDR10_PLUS = 4,              /**< HDR10 Plus格式 */
    HDR_VIVID = 5,               /**< Vivid格式 */
} HDRFormat;

/**
 * @brief HDR属性结构体定义
 *
 */
typedef struct {
    uint32_t formatCount;        /**< 支持的HDR格式的数量 */
    HDRFormat* formats;          /**< 支持的HDR格式的数组首地址 */
    float maxLum;                /**< 最大的光亮度luminance值 */
    float maxAverageLum;         /**< 最大的平均光亮度luminance值 */
    float minLum;                /**< 最小的光亮度luminance值 */
} HDRCapability;

/**
 * @brief 枚举HDR元数据关键字
 *
 */
typedef enum {
    MATAKEY_RED_PRIMARY_X = 0,                       /**< 红基色X坐标 */
    MATAKEY_RED_PRIMARY_Y = 1,                       /**< 红基色Y坐标 */
    MATAKEY_GREEN_PRIMARY_X = 2,                     /**< 绿基色X坐标 */
    MATAKEY_GREEN_PRIMARY_Y = 3,                     /**< 绿基色Y坐标 */
    MATAKEY_BLUE_PRIMARY_X = 4,                      /**< 蓝基色X坐标 */
    MATAKEY_BLUE_PRIMARY_Y = 5,                      /**< 蓝基色Y坐标 */
    MATAKEY_WHITE_PRIMARY_X = 6,                     /**< 白点X坐标 */
    MATAKEY_WHITE_PRIMARY_Y = 7,                     /**< 白点Y坐标 */
    MATAKEY_MAX_LUMINANCE = 8,                       /**< 最大的光亮度 */
    MATAKEY_MIN_LUMINANCE = 9,                       /**< 最小的光亮度 */
    MATAKEY_MAX_CONTENT_LIGHT_LEVEL = 10,            /**< 最大的内容亮度水平 */
    MATAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11,      /**< 最大的帧平均亮度水平 */
    MATAKEY_HDR10_PLUS = 12,                         /**< HDR10 Plus */
    MATAKEY_HDR_VIVID = 13,                          /**< Vivid */
} HDRMetadataKey;

/**
 * @brief HDR元数据结构体定义
 *
 */
typedef struct {
    HDRMetadataKey key;          /**< HDR元数据关键字 */
    float value;                 /**< 关键字对应的值 */
} HDRMetaData;

/**
 * @brief 用于验证内存分配信息的结构体定义
 *
 */
typedef struct {
    uint32_t width;               /**< 分配内存的宽度 */
    uint32_t height;              /**< 分配内存的高度 */
    uint64_t usage;               /**< 内存的用处 */
    PixelFormat format;           /**< 分配内存的像素格式 */
} VerifyAllocInfo;

/**
 * @brief 上屏时间戳类型枚举值
 *
 */
typedef enum {
    HARDWARE_DISPLAY_PTS_UNSUPPORTED = 0,        /**< 不支持 */
    HARDWARE_DISPLAY_PTS_DELAY = 1 << 0,         /**< Delay 时延类型 */
    HARDWARE_DISPLAY_PTS_TIMESTAMP = 1 << 1,     /**< 时间戳类型 */
} PresentTimestampType;

/**
 * @brief 上屏时间戳结构体定义
 *
 */
typedef struct {
    PresentTimestampType type;            /**< 上屏时间戳类型 */
    int64_t time;                         /**< 类型对应的值 */
} PresentTimestamp;

/**
 * @brief 扩展数据句柄结构体定义
 *
 */
typedef struct {
    int32_t fd;                  /**< 句柄 fd, -1代表不支持 */
    uint32_t reserveInts;        /**< reserve数组的个数 */
    int32_t reserve[0];          /**< reserve数组 */
} __attribute__((__packed__)) ExtDataHandle;

/**
 * @brief YUV描述信息结构体定义
 *
 */
typedef struct {
    void *baseAddr;              /**< 内存的初始地址 */
    size_t yOffset;              /**< Y的偏移量 */
    size_t uOffset;              /**< U的偏移量 */
    size_t vOffset;              /**< V的偏移量 */
    size_t yStride;              /**< Y的stride信息 */
    size_t uvStride;             /**< UV的stride信息 */
    size_t uvStep;               /**< UV的step信息 */
} __attribute__((__packed__)) YUVDescInfo;
#ifdef __cplusplus
}
#endif
#endif
/* @} */
