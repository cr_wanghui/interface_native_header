/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file DisplayComposerType.idl
 *
 * @brief 显示合成类型定义，定义显示图层合成操作相关接口所使用的数据类型。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Display模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.composer.v1_0;
sequenceable OHOS.HDI.Display.HdifdParcelable;

enum DispCmd {
    /* request cmd */
    REQUEST_CMD_PREPARE_DISPLAY_LAYERS = 64,           /**< 请求 CMD 准备显示图层 */
    REQUEST_CMD_SET_DISPLAY_CLIENT_BUFFER = 65,        /**< 请求 CMD 设置显示客户端缓冲区 */
    REQUEST_CMD_SET_DISPLAY_CLIENT_DAMAGE = 66,        /**< 请求 CMD 设置显示客户端损坏 */
    REQUEST_CMD_COMMIT = 67,                           /**< 请求 CMD 提交 */
    REQUEST_CMD_SET_LAYER_ALPHA = 68,                  /**< 请求 CMD 设置图层 ALPHA */
    REQUEST_CMD_SET_LAYER_REGION = 69,                 /**< 请求 CMD 设置图层区域 */
    REQUEST_CMD_SET_LAYER_CROP = 70,                   /**< 请求 CMD 设置图层裁剪 */
    REQUEST_CMD_SET_LAYER_ZORDER = 71,                 /**< 请求 CMD 设置图层ZORDER */
    REQUEST_CMD_SET_LAYER_PREMULTI = 72,               /**< 请求 CMD 设置图层PREMULTI */
    REQUEST_CMD_SET_LAYER_TRANSFORM_MODE = 73,         /**< 请求 CMD 设置图层变换模式 */
    REQUEST_CMD_SET_LAYER_DIRTY_REGION = 74,           /**< 请求 CMD 设置图层脏区 */
    REQUEST_CMD_SET_LAYER_VISIBLE_REGION = 75,         /**< 请求 CMD 设置图层可见区域 */
    REQUEST_CMD_SET_LAYER_BUFFER = 76,                 /**< 请求 CMD 设置图层缓冲区 */
    REQUEST_CMD_SET_LAYER_COMPOSITION_TYPE = 77,       /**< 请求 CMD 设置图层成分类型 */
    REQUEST_CMD_SET_LAYER_BLEND_TYPE = 78,             /**< 请求 CMD 设置图层混合类型 */
    REQUEST_CMD_SET_LAYER_VISIBLE = 79,                /**< 请求 CMD 设置图层可见 */
    REQUEST_CMD_SET_LAYER_MASK_INFO = 80,              /**< 请求 CMD 设置图层掩码信息 */
    REQUEST_CMD_SET_LAYER_COLOR = 81,                  /**< 请求 CMD 设置图层颜色 */
    REQUEST_CMD_BUTT,                                  /**< 请求 CMD 对接 */
    /* reply cmd */
    REPLY_CMD_SET_ERROR = 512,                         /**< 回复 CMD 设置错误 */
    REPLY_CMD_PREPARE_DISPLAY_LAYERS = 513,            /**< 回复 CMD 准备显示图层 */
    REPLY_CMD_COMMIT = 514,                            /**< 回复 CMD 提交 */
    REPLY_CMD_BUTT,                                    /**< 回复 CMD 对接*/
    /* Pack control cmd */
    CONTROL_CMD_REQUEST_BEGIN = 1024,                  /**< 控制 CMD 请求开始 */
    CONTROL_CMD_REPLY_BEGIN = 1025,                    /**< 控制 CMD 回复开始 */
    CONTROL_CMD_REQUEST_END = 1026,                    /**< 控制 CMD 请求结束 */
    CONTROL_CMD_REPLY_END = 1027,                      /**< 控制 CMD 回复结束 */
    CONTROL_CMD_BUTT                                   /**< 控制 CMD 对接 */
};

/**
 * @brief 返回值类型定义。
 *
 */
enum DispErrCode {
    DISPLAY_SUCCESS = 0,             /**< 成功 */
    DISPLAY_FAILURE = -1,            /**< 失败 */
    DISPLAY_FD_ERR = -2,             /**< Fd错误 */
    DISPLAY_PARAM_ERR = -3,          /**< 参数错误 */
    DISPLAY_NULL_PTR = -4,           /**< 空指针 */
    DISPLAY_NOT_SUPPORT = -5,        /**< 不支持的特性 */
    DISPLAY_NOMEM = -6,              /**< 内存不足 */
    DISPLAY_SYS_BUSY = -7,           /**< 系统繁忙 */
    DISPLAY_NOT_PERM = -8,           /**< 操作不允许 */
};

/**
 * @brief 像素格式类型定义。
 *
 */
enum PixelFormat {
    PIXEL_FMT_CLUT8 = 0,                 /**< CLUT8 格式 */
    PIXEL_FMT_CLUT1,                     /**< CLUT1 格式 */
    PIXEL_FMT_CLUT4,                     /**< CLUT4 格式 */
    PIXEL_FMT_RGB_565,                   /**< RGB565 格式 */
    PIXEL_FMT_RGBA_5658,                 /**< RGBA5658 格式 */
    PIXEL_FMT_RGBX_4444,                 /**< RGBX4444 格式 */
    PIXEL_FMT_RGBA_4444,                 /**< RGBA4444 格式 */
    PIXEL_FMT_RGB_444,                   /**< RGB444 格式 */
    PIXEL_FMT_RGBX_5551,                 /**< RGBX5551 格式 */
    PIXEL_FMT_RGBA_5551,                 /**< RGBA5551 格式 */
    PIXEL_FMT_RGB_555,                   /**< RGB555 格式 */
    PIXEL_FMT_RGBX_8888,                 /**< RGBX8888 格式 */
    PIXEL_FMT_RGBA_8888,                 /**< RGBA8888 格式 */
    PIXEL_FMT_RGB_888,                   /**< RGB888 格式 */
    PIXEL_FMT_BGR_565,                   /**< BGR565 格式 */
    PIXEL_FMT_BGRX_4444,                 /**< BGRX4444 格式 */
    PIXEL_FMT_BGRA_4444,                 /**< BGRA4444 格式 */
    PIXEL_FMT_BGRX_5551,                 /**< BGRX5551 格式 */
    PIXEL_FMT_BGRA_5551,                 /**< BGRA5551 格式 */
    PIXEL_FMT_BGRX_8888,                 /**< BGRX8888 格式 */
    PIXEL_FMT_BGRA_8888,                 /**< BGRA8888 格式 */
    PIXEL_FMT_YUV_422_I,                 /**< YUV422  交错格式 */
    PIXEL_FMT_YCBCR_422_SP,              /**< YCBCR422 半平面格式 */
    PIXEL_FMT_YCRCB_422_SP,              /**< YCRCB422 半平面格式 */
    PIXEL_FMT_YCBCR_420_SP,              /**< YCBCR420 半平面格式 */
    PIXEL_FMT_YCRCB_420_SP,              /**< YCRCB420 半平面格式 */
    PIXEL_FMT_YCBCR_422_P,               /**< YCBCR422 平面格式 */
    PIXEL_FMT_YCRCB_422_P,               /**< YCRCB422 平面格式 */
    PIXEL_FMT_YCBCR_420_P,               /**< YCBCR420 平面格式 */
    PIXEL_FMT_YCRCB_420_P,               /**< YCRCB420 平面格式 */
    PIXEL_FMT_YUYV_422_PKG,              /**< YUYV422 平面格式 */
    PIXEL_FMT_UYVY_422_PKG,              /**< UYVY422 平面格式 */
    PIXEL_FMT_YVYU_422_PKG,              /**< YVYU422 平面格式 */
    PIXEL_FMT_VYUY_422_PKG,              /**< VYUY422 平面格式 */
    PIXEL_FMT_RGBA_1010102,              /**< RGBA_1010102 供应商格式*/
    PIXEL_FMT_VENDER_MASK = 0X7FFF0000,  /**< 供应商掩码 格式 */
    PIXEL_FMT_BUTT = 0X7FFFFFFF           /**< Invalid 像素格式 */
};

/* *
 * @brief 定义缓冲区使用情况
 *
 */
enum BufferUsage : unsigned long {
    HBM_USE_CPU_READ = (1ULL << 0),        /**< CPU 读取内存 */
    HBM_USE_CPU_WRITE = (1ULL << 1),       /**< CPU 写入内存 */
    HBM_USE_MEM_MMZ = (1ULL << 2),         /**< 媒体内存区 (MMZ) */
    HBM_USE_MEM_DMA = (1ULL << 3),         /**< 直接内存访问 （DMA） 内存区 */
    HBM_USE_MEM_SHARE = (1ULL << 4),       /**< 共享内存内存区*/
    HBM_USE_MEM_MMZ_CACHE = (1ULL << 5),   /**< 存在缓存的 MMZ*/
    HBM_USE_MEM_FB = (1ULL << 6),          /**< 帧内存 */
    HBM_USE_ASSIGN_SIZE = (1ULL << 7),     /**< 分配内存 */
    HBM_USE_HW_RENDER = (1ULL << 8),       /**< 写入GPU内存情况 */
    HBM_USE_HW_TEXTURE = (1ULL << 9),      /**< 读取GPU内存情况 */
    HBM_USE_HW_COMPOSER = (1ULL << 10),    /**< 硬件编写情况 */
    HBM_USE_PROTECTED = (1ULL << 11),      /**< 安全缓冲区情况，例如 DRM */
    HBM_USE_CAMERA_READ = (1ULL << 12),    /**< 读取相机情况 */
    HBM_USE_CAMERA_WRITE = (1ULL << 13),   /**< 写入相机情况 */
    HBM_USE_VIDEO_ENCODER = (1ULL << 14),  /**< 编码情况 */
    HBM_USE_VIDEO_DECODER = (1ULL << 15),  /**< 解码情况 */
    HBM_USE_CPU_READ_OFTEN = (1ULL << 16), /**< HBM 经常使用 CPU 读取情况 */
    HBM_USE_VENDOR_PRI0 = (1ULL << 44),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI1 = (1ULL << 45),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI2 = (1ULL << 46),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI3 = (1ULL << 47),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI4 = (1ULL << 48),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI5 = (1ULL << 49),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI6 = (1ULL << 50),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI7 = (1ULL << 51),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI8 = (1ULL << 52),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI9 = (1ULL << 53),    /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI10 = (1ULL << 54),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI11 = (1ULL << 55),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI12 = (1ULL << 56),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI13 = (1ULL << 57),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI14 = (1ULL << 58),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI15 = (1ULL << 59),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI16 = (1ULL << 60),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI17 = (1ULL << 61),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI18 = (1ULL << 62),   /**< 为供应商提供 */
    HBM_USE_VENDOR_PRI19 = (1ULL << 63),   /**< 为供应商提供 */
};

/**
 * @brief 枚举图像的转换类型
 *
 */
enum TransformType {
    ROTATE_NONE = 0,                /**< 不旋转 */
    ROTATE_90,                      /**< 旋转90度 */
    ROTATE_180,                     /**< 旋转180度 */
    ROTATE_270,                     /**< 旋转270度 */
    MIRROR_H,                       /**< 水平方向镜像转换 */
    MIRROR_V,                       /**< 垂直方向镜像转换 */
    MIRROR_H_ROTATE_90,             /**< 水平方向镜像转换, 旋转90度 */
    MIRROR_V_ROTATE_90,             /**< 垂直方向镜像转换, 旋转90度 */
    ROTATE_BUTT                     /**< 无效操作 */
};

/**
 * @brief 枚举显示状态。
 */
enum DispPowerStatus {
    POWER_STATUS_ON = 0,            /**< 上电模式 */
    POWER_STATUS_STANDBY = 1,       /**< 待机模式 */
    POWER_STATUS_SUSPEND = 2,       /**< 休眠模式 */
    POWER_STATUS_OFF = 3,           /**< 下电模式 */
    POWER_STATUS_BUTT               /**< 默认模式 */
};

/**
 * @brief 枚举特殊层的组合类型。
 */
enum CompositionType {
    COMPOSITION_CLIENT,       /**< Client 合成类型，使用CPU或者GPU合成。 */
    COMPOSITION_DEVICE,       /**< Device 合成类型，使用Device合成。 */
    COMPOSITION_CURSOR,       /**< Cursor合成类型，用于光标合成。 */
    COMPOSITION_VIDEO,        /**< Video合成类型，用于视频层合成。 */
    COMPOSITION_DEVICE_CLEAR, /**< Device清除合成类型, 用于清楚Device*/
    COMPOSITION_CLIENT_CLEAR, /**< Client清除合成类型, 用于清除Client*/
    COMPOSITION_TUNNEL,       /**< Tunnel合成类型, 用于tunnel合成. */
    COMPOSITION_BUTT          /**< 合成类型, 一个不可用类型，用于默认初始化。 */
};

/**
 * @brief 图层类型定义。
 *
 */
enum LayerType {
    LAYER_TYPE_GRAPHIC,             /**< 图形层 */
    LAYER_TYPE_OVERLAY,             /**< 视频层 */
    LAYER_TYPE_SDIEBAND,            /**< 媒体播放 */
    LAYER_TYPE_CURSOR,              /**< 光标层 */
    LAYER_TYPE_BUTT                 /**< 空图层 */
};

/**
 * @brief 支持的图像混合类型。
 *
 * 系统在硬件加速期间基于指定的混合类型合成图像。
 *
 */
enum BlendType {
    BLEND_NONE = 0,             /**< No 混合操作 */
    BLEND_CLEAR,                /**< CLEAR 混合操作 */
    BLEND_SRC,                  /**< SRC 混合操作 */
    BLEND_SRCOVER,              /**< SRC_OVER 混合操作 */
    BLEND_DSTOVER,              /**< DST_OVER 混合操作 */
    BLEND_SRCIN,                /**< SRC_IN 混合操作 */
    BLEND_DSTIN,                /**< DST_IN 混合操作 */
    BLEND_SRCOUT,               /**< SRC_OUT 混合操作 */
    BLEND_DSTOUT,               /**< DST_OUT 混合操作 */
    BLEND_SRCATOP,              /**< SRC_ATOP 混合操作 */
    BLEND_DSTATOP,              /**< DST_ATOP 混合操作 */
    BLEND_ADD,                  /**< ADD 混合操作 */
    BLEND_XOR,                  /**< XOR 混合操作 */
    BLEND_DST,                  /**< DST 混合操作 */
    BLEND_AKS,                  /**< AKS 混合操作 */
    BLEND_AKD,                  /**< AKD 混合操作 */
    BLEND_BUTT                  /**< 空操作 */
};

/**
 * @brief 硬件加速支持的ROP操作类型。
 *
  * 硬件加速支持的ROP操作类型，在将前景位图的RGB颜色分量和Alpha分量值与背景位图的RGB颜色
 * 分量值和Alpha分量值进行按位的布尔运算（包括按位与，按位或等），将结果输出。
 *
 */
enum RopType {
    ROP_BLACK = 0,                  /**< 黑色 */
    ROP_NOTMERGEPEN,                /**< ~(S2+S1) */
    ROP_MASKNOTPEN,                 /**< ~S2&S1 */
    ROP_NOTCOPYPEN,                 /**< ~S2 */
    ROP_MASKPENNOT,                 /**< S2&~S1 */
    ROP_NOT,                        /**< ~S1 */
    ROP_XORPEN,                     /**< S2^S1 */
    ROP_NOTMASKPEN,                 /**< ~(S2&S1) */
    ROP_MASKPEN,                    /**< S2&S1 */
    ROP_NOTXORPEN,                  /**< ~(S2^S1) */
    ROP_NOP,                        /**< S1 */
    ROP_MERGENOTPEN,                /**< ~S2+S1 */
    ROP_COPYPE,                     /**< S2 */
    ROP_MERGEPENNOT,                /**< S2+~S1 */
    ROP_MERGEPEN,                   /**< S2+S1 */
    ROP_WHITE,                      /**< 白色 */
    ROP_BUTT                        /**< 无效值 */
};

/**
 * @brief Colorkey操作类型定义，即硬件加速支持的Colorkey操作类型。
 *
 */
enum ColorKey {
    CKEY_NONE = 0,               /**< 不使用Colorkey */
    CKEY_SRC,                    /**< 使用源Colorkey */
    CKEY_DST,                    /**< 使用目标Colorkey */
    CKEY_BUTT                    /**< 空操作 */
};

/**
 * @brief 硬件加速支持的镜像操作类型定义。
 *
 */
enum MirrorType {
    MIRROR_NONE = 0,         /**< 不使用镜像 */
    MIRROR_LR,               /**< 左右镜像 */
    MIRROR_TB,               /**< 上下镜像 */
    MIRROR_BUTT              /**< 空操作 */
};

/**
 * @brief 热插拔连接类型定义。
 *
 */
enum Connection {
    CON_INVALID = 0,             /**< 无效类型 */
    CONNECTED,                   /**< 已连接 */
    DISCONNECTED                 /**< 断开连接 */
};

/**
 * @brief 显示接口类型。
 *
 */
enum InterfaceType {
    DISP_INTF_HDMI = 0,             /**< HDMI 接口 */
    DISP_INTF_LCD,                  /**< LCD 接口 */
    DISP_INTF_BT1120,               /**< BT1120 接口 */
    DISP_INTF_BT656,                /**< BT656 接口 */
    DISP_INTF_YPBPR,                /**< YPBPR 接口 */
    DISP_INTF_RGB,                  /**< RGB 接口 */
    DISP_INTF_CVBS,                 /**< CVBS 接口 */
    DISP_INTF_SVIDEO,               /**< SVIDEO 接口 */
    DISP_INTF_VGA,                  /**< VGA 接口 */
    DISP_INTF_MIPI,                 /**< MIPI 接口 */
    DISP_INTF_PANEL,                /**< PANEL 接口 */
    DISP_INTF_BUTT,                 /**< BUTT 接口, 一个不可用类型, 用于默认初始化。 */
};

/**
 * @brief 定义包含名称、属性ID和值的属性对象。
 *
 */
struct PropertyObject {
    String name;             /**< 属性名称 */
    unsigned int propId;     /**< 属性ID */
    unsigned long value;     /**< 属性值 */
};

/**
 * @brief 定义输出性能。
 */
struct DisplayCapability {
    String name;                           /**< 显示设备名称 */
    enum InterfaceType type;               /**< 显示屏接口类型 */
    unsigned int phyWidth;                 /**< 物理宽度 */
    unsigned int phyHeight;                /**< 物理高度 */
    unsigned int supportLayers;            /**< 支持的图层数 */
    unsigned int virtualDispCount;         /**< 支持的虚拟屏数 */
    boolean supportWriteBack;              /**< 是否支持回写 */
    unsigned int propertyCount;            /**< 属性数组大小 */
    struct PropertyObject[] props;         /**< 属性数组 */
};

/**
 * @brief 定义输出模式信息。
 */
struct DisplayModeInfo {
    int width;                      /**< 像素宽度 */
    int height;                     /**< 像素高度 */
    unsigned int freshRate;         /**< 刷新速率 */
    int id;                         /**< 模式ID */
};

/**
 * @brief Defines 定义图层信息结构体。
 *
 * 在创建图层时，需要将LayerInfo传递给创建图层接口，创建图层接口根据图层信息创建相应图层。
 *
 */
struct LayerInfo {
    int width;                        /**< 图层宽度 */
    int height;                       /**< 图层高度 */
    enum LayerType type;              /**< 图层类型，包括图形层、视频层和媒体播放模式。 */
    int bpp;                          /**< 每像素所占Bit数 */
    enum PixelFormat pixFormat;       /**< 图层像素格式 */
};

/**
 * @brief 定义图层Alpha信息的结构体。
 *
 */
struct LayerAlpha {
    boolean enGlobalAlpha;    /**< 全局Alpha使能标志 */
    boolean enPixelAlpha;     /**< 像素Alpha使能标志 */
    unsigned char alpha0;     /**< Alpha0值，取值范围：[0, 255]。 */
    unsigned char alpha1;     /**< Alpha1值，取值范围：[0, 255]。 */
    unsigned char gAlpha;     /**< 全局Alpha值，取值范围：[0, 255]。 */
};

/**
 * @brief 定义矩形框信息。
 *
 */
struct IRect {
    int x;    /**< 矩形框起始x坐标 */
    int y;    /**< 矩形框起始y坐标 */
    int w;    /**< 矩形框宽度 */
    int h;    /**< 矩形框高度 */
};

/**
 * @brief 用于存放窗口相关信息的结构体定义，提供给硬件加速使用，例如图像合成，位图搬移等操作。
 */
struct ISurface {
    unsigned long phyAddr;             /**< 图像首地址 */
    int height;                        /**< 图像高度 */
    int width;                         /**< 图像宽度 */
    int stride;                        /**< 图像跨度 */
    enum PixelFormat enColorFmt;       /**< 图像格式 */
    boolean bYCbCrClut;                /**< CLUT表是否位于 YCbCr 空间 */
    boolean bAlphaMax255;              /**< 图像Alpha最大值为255还是128 */
    boolean bAlphaExt1555;             /**< 是否使能1555的Alpha扩展 */
    unsigned char alpha0;              /**< Alpha0值，取值范围：[0,255]。 */
    unsigned char alpha1;              /**< Alpha1值，取值范围：[0,255]。 */
    unsigned long cbcrPhyAddr;         /**< CbCr分量地址 */
    int cbcrStride;                    /**< CbCr分量跨度 */
    unsigned long clutPhyAddr;         /**< Clut表首地址，用作颜色扩展或颜色校正。 */
};

/**
 * @brief 线条描述结构体定义，用于硬件加速绘制直线。
 *
 */
struct ILine {
    int x0;                         /**< 线条起点的X坐标 */
    int y0;                         /**< 线条起点的Y坐标 */
    int x1;                         /**< 线条终点的X坐标 */
    int y1;                         /**< 线条终点的Y坐标 */
    unsigned int color;             /**< 线条颜色 */
};

/**
 * @brief 圆形描述结构体定义，用于硬件加速绘制圆形。
 *
 */
struct ICircle {
    int x;                         /**< 圆心X坐标 */
    int y;                         /**< 圆心Y坐标 */
    int r;                         /**< 圆的半径 */
    unsigned int color;            /**< 圆的颜色 */
};

/**
 * @brief 矩形描述结构体定义，用于硬件加速绘制矩形，
 *
 */
struct Rectangle {
    struct IRect rect;            /**< 矩形区域 */
    unsigned int color;           /**< 矩形颜色 */
};

/**
 * @brief 图像硬件加速相关的操作选项结构体定义，用于图像硬件加速时的操作选项。
 *
 */
struct GfxOpt {
    boolean enGlobalAlpha;             /**< 全局Alpha使能位 */
    unsigned int globalAlpha;          /**< 全局Alpha的值 */
    boolean enPixelAlpha;              /**< 像素Alpha使能位 */
    enum BlendType blendType;          /**< 混合方式 */
    enum ColorKey colorKeyFrom;        /**< 色键模式 */
    boolean enableRop;                 /**< Rop功能使能位 */
    enum RopType colorRopType;         /**< 颜色的Rop类型 */
    enum RopType alphaRopType;         /**< Alpha的Rop类型 */
    boolean enableScale;               /**< 缩放功能使能位 */
    enum TransformType rotateType;     /**< 旋转类型 */
    enum MirrorType mirrorType;        /**< 镜像类型 */
};

/**
 * @brief  色域类型枚举值。
 */
enum ColorGamut {
    COLOR_GAMUT_INVALID = -1,           /**< 无效值 */
    COLOR_GAMUT_NATIVE = 0,             /**< 默认值 */
    COLOR_GAMUT_STANDARD_BT601 = 1,     /**< Standard BT601类型 */
    COLOR_GAMUT_STANDARD_BT709 = 2,     /**< Standard BT709类型 */
    COLOR_GAMUT_DCI_P3 = 3,             /**< DCI P3类型 */
    COLOR_GAMUT_SRGB = 4,               /**< SRGB类型 */
    COLOR_GAMUT_ADOBE_RGB = 5,          /**< Adobe RGB类型 */
    COLOR_GAMUT_DISPLAY_P3 = 6,         /**< display P3类型 */
    COLOR_GAMUT_BT2020 = 7,             /**< BT2020类型 */
    COLOR_GAMUT_BT2100_PQ = 8,          /**< BT2100 PQ类型 */
    COLOR_GAMUT_BT2100_HLG = 9,         /**< BT2100 HLG类型 */
    COLOR_GAMUT_DISPLAY_BT2020 = 10,    /**< Display BT2020类型 */
};

/**
 * @brief 枚举色域的映射类型。
 *
 */
enum GamutMap {
    GAMUT_MAP_CONSTANT = 0,             /**< 不变 */
    GAMUT_MAP_EXPANSION = 1,            /**< 映射增强 */
    GAMUT_MAP_HDR_CONSTANT = 2,         /**< 不变，用于HDR。 */
    GAMUT_MAP_HDR_EXPANSION = 3,        /**< 映射增强，用于HDR。 */
};

/**
 * @brief 枚举颜色空间的类型。
 *
 */
enum ColorDataSpace {
    COLOR_DATA_SPACE_UNKNOWN = 0,                 /**< 未知的 */
    GAMUT_BT601 = 0x00000001,                     /**< BT601色域 */
    GAMUT_BT709 = 0x00000002,                     /**< BT709色域 */
    GAMUT_DCI_P3 = 0x00000003,                    /**< DCI_P3色域 */
    GAMUT_SRGB = 0x00000004,                      /**< SRGB色域 */
    GAMUT_ADOBE_RGB = 0x00000005,                 /**< ADOBE_RGB色域 */
    GAMUT_DISPLAY_P3 = 0x00000006,                /**< DISPLAY_P3色域 */
    GAMUT_BT2020 = 0x00000007,                    /**< BT2020色域 */
    GAMUT_BT2100_PQ = 0x00000008,                 /**< BT2100_PQ色域 */
    GAMUT_BT2100_HLG = 0x00000009,                /**< BT2100_HLG色域 */
    GAMUT_DISPLAY_BT2020 = 0x0000000a,            /**< DISPLAY_BT2020色域 */
    TRANSFORM_FUNC_UNSPECIFIED = 0x00000100,      /**< UNSPECIFIED转换函数 */
    TRANSFORM_FUNC_LINEAR = 0x00000200,           /**< LINEAR转换函数 */
    TRANSFORM_FUNC_SRGB = 0x00000300,             /**< SRGB转换函数 */
    TRANSFORM_FUNC_SMPTE_170M = 0x00000400,       /**< SMPTE_170M转换函数 */
    TRANSFORM_FUNC_GM2_2 = 0x00000500,            /**< GM2_2转换函数 */
    TRANSFORM_FUNC_GM2_6 = 0x00000600,            /**< GM2_6转换函数 */
    TRANSFORM_FUNC_GM2_8 = 0x00000700,            /**< GM2_8转换函数 */
    TRANSFORM_FUNC_ST2084 = 0x00000800,           /**< ST2084转换函数 */
    TRANSFORM_FUNC_HLG = 0x00000900,              /**< HLG转换函数 */
    PRECISION_UNSPECIFIED = 0x00010000,           /**< UNSPECIFIED精度 */
    PRECISION_FULL = 0x00020000,                  /**< FULL精度 */
    PRESION_LIMITED = 0x00030000,                 /**< LIMITED精度 */
    PRESION_EXTENDED = 0x00040000,                /**< EXTENDED精度 */
    BT601_SMPTE170M_FULL = 1 | 1024 | 131072,     /**< BT601色域 | SMPTE_170M转换函数 | FULL精度 */
    BT601_SMPTE170M_LIMITED = 1 | 1024 | 196608,  /**< BT601色域 | SMPTE_170M转换函数 | LIMITED精度 */
    BT709_LINEAR_FULL = 2 | 512 | 131072,         /**< BT709色域 | LINEAR转换函数 | FULL精度 */
    BT709_LINEAR_EXTENDED = 2 | 512 | 262144,     /**< BT709色域 | LINEAR转换函数 | EXTENDED精度 */
    BT709_SRGB_FULL = 2 | 768 | 131072,           /**< BT709色域 | SRGB转换函数 | FULL精度 */
    BT709_SRGB_EXTENDED = 2 | 768 | 262144,       /**< BT709色域 | SRGB转换函数 | EXTENDED精度 */
    BT709_SMPTE170M_LIMITED = 2 | 1024 | 196608,  /**< BT709色域 | SMPTE_170M转换函数 | LIMITED精度 */
    DCI_P3_LINEAR_FULL = 3 | 512 | 131072,        /**< DCI_P3色域 | LINEAR转换函数 | FULL精度 */
    DCI_P3_GAMMA26_FULL = 3 | 1536 | 131072,      /**< DCI_P3色域 | GM2_6转换函数 | FULL精度 */
    DISPLAY_P3_LINEAR_FULL = 6 | 512 | 131072,    /**< DISPLAY_P3色域 | LINEAR转换函数 | FULL精度 */
    DCI_P3_SRGB_FULL = 3 | 768 | 131072,          /**< DCI_P3色域 | SRGB转换函数 | FULL精度 */
    ADOBE_RGB_GAMMA22_FULL = 5 | 1280 | 131072,   /**< ADOBE_RGB色域 | GM2_2转换函数 | FULL精度 */
    BT2020_LINEAR_FULL = 7 | 512 | 131072,        /**< BT2020色域 | LINEAR转换函数 | FULL精度 */
    BT2020_SRGB_FULL = 7 | 768 | 131072,          /**< BT2020色域 | SRGB转换函数 | FULL精度 */
    BT2020_SMPTE170M_FULL = 7 | 1024 | 131072,    /**< BT2020色域 | SMPTE_170M转换函数 | FULL精度 */
    BT2020_ST2084_FULL = 7 | 2048 | 131072,       /**< BT2020色域 | ST2084转换函数 | FULL精度 */
    BT2020_HLG_FULL = 7 | 2304 | 131072,          /**< BT2020色域 | HLG转换函数 | FULL精度 */
    BT2020_ST2084_LIMITED = 7 | 2048 | 196608,    /**< BT2020色域 | ST2084转换函数 | LIMITED精度 */
};

/**
 * @brief 枚举HDR格式。
 *
 */
enum HDRFormat {
    NOT_SUPPORT_HDR = 0,                      /**< 不支持HDR */  
    DOLBY_VISION = 1,                         /**< Dolby Vision格式 */
    HDR10 = 2,                                /**< HDR10格式 */
    HLG = 3,                                  /**< HLG格式 */
    HDR10_PLUS = 4,                           /**< HDR10 Plus格式 */
    HDR_VIVID = 5,                            /**< Vivid格式 */
};

/**
 * @brief HDR属性结构体定义。
 *
 */
struct HDRCapability {
    unsigned int formatCount;        /**< 支持的HDR格式的数量 */
    enum HDRFormat[] formats;        /**< 支持的HDR格式的数组首地址 */
    float maxLum;                    /**< 最大光亮度luminance值 */
    float maxAverageLum;             /**< 平均光亮度luminance值 */
    float minLum;                    /**< 最小光亮度luminance值 */
};

/**
 * @brief 枚举HDR元数据关键字。
 *
 */
enum HDRMetadataKey {
    MATAKEY_RED_PRIMARY_X = 0,                  /**< 红基色X坐标 */   
    MATAKEY_RED_PRIMARY_Y = 1,                  /**< 红基色Y坐标 */
    MATAKEY_GREEN_PRIMARY_X = 2,                /**< 绿基色X坐标 */
    MATAKEY_GREEN_PRIMARY_Y = 3,                /**< 绿基色Y坐标 */
    MATAKEY_BLUE_PRIMARY_X = 4,                 /**< 蓝基色X坐标 */
    MATAKEY_BLUE_PRIMARY_Y = 5,                 /**< 蓝基色Y坐标 */
    MATAKEY_WHITE_PRIMARY_X = 6,                /**< 白点X坐标 */
    MATAKEY_WHITE_PRIMARY_Y = 7,                /**< 白点Y坐标 */
    MATAKEY_MAX_LUMINANCE = 8,                  /**< 最大的光亮度 */    
    MATAKEY_MIN_LUMINANCE = 9,                  /**< 最小的光亮度 */
    MATAKEY_MAX_CONTENT_LIGHT_LEVEL = 10,       /**< 最大的内容亮度水平 */
    MATAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11, /**< 最大的帧平均亮度水平 */
    MATAKEY_HDR10_PLUS = 12,                    /**< HDR10 Plus */
    MATAKEY_HDR_VIVID = 13,                     /**< Vivid */
};

/**
 * @brief HDR元数据结构体定义。
 *
 */
struct HDRMetaData {
    enum HDRMetadataKey key;        /**< HDR元数据关键字 */
    float value;                    /**< 关键字对应的值 */
};

/**
 * @brief 上屏时间戳类型枚举值。
 *
 */
enum PresentTimestampType {
    HARDWARE_DISPLAY_PTS_UNSUPPORTED = 0,        /**< 不支持 */
    HARDWARE_DISPLAY_PTS_DELAY = 1 << 0,         /**< 时延类型 */
    HARDWARE_DISPLAY_PTS_TIMESTAMP = 1 << 1,     /**< 时间戳类型 */
};

/**
 * @brief 图层蒙版枚举值。
 *
 */
enum MaskInfo {
    LAYER_NORAML = 0,
    LAYER_HBM_SYNC = 1,
};

/**
 * @brief 上屏时间戳结构体定义。
 *
 */
struct PresentTimestamp {
    enum PresentTimestampType type;          /**< 上屏时间戳类型 */
    long time;                               /**< 类型对应的值 */
};

/**
 * @brief 扩展数据句柄结构体定义。
 *
 */
struct ExtDataHandle {
    int fd;                          /**< 句柄 Fd, -1代表不支持 */
    unsigned int reserveInts;        /**< Reserve数组的个数 */
    int[] reserve;                   /**< Reserve数组 */
};

/**
 * @brief YUV描述信息结构体定义。
 *
 */
struct YUVDescInfo {
    unsigned long baseAddr;           /**< 内存的初始地址 */
    unsigned int yOffset;             /**< Y的偏移量 */
    unsigned int uOffset;             /**< U的偏移量 */
    unsigned int vOffset;             /**< V的偏移量 */
    unsigned int yStride;             /**< Y的Stride信息 */
    unsigned int uvStride;            /**< UV的Stride信息 */
    unsigned int uvStep;              /**< UV的Step信息 */
};

/**
 * @brief 定义 hdi fd 信息结构体。
 *
 */
struct HdifdInfo {
    int id;                  /**<  fd对应id*/
    HdifdParcelable hdiFd;   /**<  fd具体信息*/
};

/**
 * @brief 定义图层颜色设置的结构结构体
 *
 */
struct LayerColor {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
};
/** @} */