/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file ICameraDevice.idl
 *
 * @brief Camera设备操作接口。
 *
 * @since 4.1
 * @version 1.2
 */
 
/**
 * @brief Camera设备接口的包路径。
 *
 * @since 4.1
 * @version 1.2
 */
package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_1.ICameraDevice;
import ohos.hdi.camera.v1_2.IStreamOperatorCallback;
import ohos.hdi.camera.v1_2.IStreamOperator;

/**
 * @brief 定义Camera设备基本的操作。
 *
 * 获取流操作句柄，获取动态能力值等操作。
 */
interface ICameraDevice extends ohos.hdi.camera.v1_1.ICameraDevice {
     /**
     * @brief 获取流操作句柄。
     *
     * @param callbackObj 设置流回调接口，详细可查看{@link IStreamOperatorCallback}，
     * 用于上报捕获开始{@link OnCaptureStarted}，捕获结束{@link OnCaptureEnded}，
     * 捕获错误等信息{@link OnCaptureError}。
     * @param streamOperator 返回流操作句柄。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    GetStreamOperator_V1_2([in] IStreamOperatorCallback callbackObj, [out] IStreamOperator streamOperator);

    /**
     * @brief 获取动态能力值。
     *
     * @param metaIn 能力输入。
     * @param metaOut 能力输出。
     * 捕获错误等信息{@link OnCaptureError}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    GetStatus([in] unsigned char[] metaIn, [out] unsigned char[] metaOut);
}
/** @} */