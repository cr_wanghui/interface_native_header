
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraDeviceCallback.idl
 *
 * @brief Camera设备的回调接口，主要包含Camera设备发生错误时和上报metadata的回调函数。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Camera设备接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief 定义Camera设备回调操作。
 *
 * 设置回调接口、返回错误信息和相关的metadata的回调。
 */
[callback] interface ICameraDeviceCallback {

     /**
     * @brief 设备发生错误时调用，由调用者实现，用于返回错误信息给调用者。
     *
     * @param type 错误类型，具体可参考{@link ErrorType}。
     * @param errorCode 错误码，当前暂未使用。
     *
     * @since 3.2
     * @version 1.0
     */
    OnError([in] enum ErrorType type, [in] int errorCode);

    /**
     * @brief 上报Camera设备相关的metadata的回调，上报方式查看{@link SetResultMode}。
     *
     * @param timestamp metadata上报的时间戳。
     * @param result 上报的metadata，由{@link EnableResult}指定，
     * 可通过{@link GetEnabledResults}查询，{@link DisableResult}关闭上报开关。
     *
     * @since 3.2
     * @version 1.0
     */
    OnResult([in] unsigned long timestamp, [in] unsigned char[] result);
}
/** @} */