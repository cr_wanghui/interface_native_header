/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.hdi.codec.v2_0;

import ohos.hdi.codec.v2_0.CodecTypes;

/**
 * @brief 视频编码格式枚举，对OMX原生枚举OMX_VIDEO_CODINGTYPE的补充。
 */
enum CodecVideoExType {
    CODEC_VIDEO_CodingVP9  = 10, /**< VP9格式 */
    CODEC_VIDEO_CodingHEVC = 11, /**< HEVC格式 */
};

/**
 * @brief HEVC的profile枚举。
 */
enum CodecHevcProfile {
    CODEC_HEVC_PROFILE_INVALID = 0x0,                       /**< 无效的profile */
    CODEC_HEVC_PROFILE_MAIN = 0x1,                          /**< main profile */
    CODEC_HEVC_PROFILE_MAIN10 = 0x2,                        /**< main10 profile */
    CODEC_HEVC_PROFILE_MAIN_STILL = 0x3,                    /**< main still profile */
    // 支持main_10 profile以及HDR SEI.
    CODEC_HEVC_PROFILE_MAIN10_HDR10 = 0x1000,               /**< main10 hdr10 profile */
    CODEC_HEVC_PROFILE_MAIN10_HDR10_PLUS = 0x2000,          /**< main10 hdr10 plus profile */
    CODEC_HEVC_PROFILE_MAX = 0x7FFFFFFF                     /**< 最大值 */
};

/**
 * @brief HEVC的level枚举。
 */
enum CodecHevcLevel {
    CODEC_HEVC_LEVEL_INVALID = 0x0,                        /**< 无效的level */
    CODEC_HEVC_MAIN_TIER_LEVEL1 = 0x1,                     /**< main tier level1 */
    CODEC_HEVC_HIGH_TIER_LEVEL1 = 0x2,                     /**< high tier level1 */
    CODEC_HEVC_MAIN_TIER_LEVEL2 = 0x4,                     /**< main tier level2 */
    CODEC_HEVC_HIGH_TIER_LEVEL2 = 0x8,                     /**< high tier level2 */
    CODEC_HEVC_MAIN_TIER_LEVEL21 = 0x10,                   /**< main tier level2.1 */
    CODEC_HEVC_HIGH_TIER_LEVEL21 = 0x20,                   /**< high tier level2.1 */
    CODEC_HEVC_MAIN_TIER_LEVEL3 = 0x40,                    /**< main tier level3 */
    CODEC_HEVC_HIGH_TIER_LEVEL3 = 0x80,                    /**< high tier level3 */
    CODEC_HEVC_MAIN_TIER_LEVEL31 = 0x100,                  /**< main tier level3.1 */
    CODEC_HEVC_HIGH_TIER_LEVEL31 = 0x200,                  /**< high tier level3.1 */
    CODEC_HEVC_MAIN_TIER_LEVEL4 = 0x400,                   /**< main tier level4 */
    CODEC_HEVC_HIGH_TIER_LEVEL4 = 0x800,                   /**< high tier level4 */
    CODEC_HEVC_MAIN_TIER_LEVEL41 = 0x1000,                 /**< main tier level4.1 */
    CODEC_HEVC_HIGH_TIER_LEVEL41 = 0x2000,                 /**< high tier level4.1 */
    CODEC_HEVC_MAIN_TIER_LEVEL5 = 0x4000,                  /**< main tier level5 */
    CODEC_HEVC_HIGH_TIER_LEVEL5 = 0x8000,                  /**< high tier level5 */
    CODEC_HEVC_MAIN_TIER_LEVEL51 = 0x10000,                /**< main tier level5.1 */
    CODEC_HEVC_HIGH_TIER_LEVEL51 = 0x20000,                /**< high tier level5.1 */
    CODEC_HEVC_MAIN_TIER_LEVEL52 = 0x40000,                /**< main tier level5.2 */
    CODEC_HEVC_HIGH_TIER_LEVEL52 = 0x80000,                /**< high tier level5.2 */
    CODEC_HEVC_MAIN_TIER_LEVEL6 = 0x100000,                /**< main tier level6 */
    CODEC_HEVC_HIGH_TIER_LEVEL6 = 0x200000,                /**< high tier level6 */
    CODEC_HEVC_MAIN_TIER_LEVEL61 = 0x400000,               /**< main tier level6.1 */
    CODEC_HEVC_HIGH_TIER_LEVEL61 = 0x800000,               /**< high tier level6.1 */
    CODEC_HEVC_MAIN_TIER_LEVEL62 = 0x1000000,              /**< main tier level6.2 */
    CODEC_HEVC_HIGH_TIER_LEVEL62 = 0x2000000,              /**< high tier level6.2 */
    CODEC_HEVC_HIGH_TIER_MAX = 0x7FFFFFFF                  /**< 最大值 */
};

/**
 * @brief 用于存储编解码视频帧的buffer类型
 */
enum CodecBufferType {
    /** 无效的buffer类型 */
    CODEC_BUFFER_TYPE_INVALID = 0,
    /** Virtual address type */
    CODEC_BUFFER_TYPE_VIRTUAL_ADDR = 0x1,
    /** 共享内存 */
    CODEC_BUFFER_TYPE_AVSHARE_MEM_FD = 0x2,
    /** Handle. */
    CODEC_BUFFER_TYPE_HANDLE = 0x4,
    /** Dynamic handle. */
    CODEC_BUFFER_TYPE_DYNAMIC_HANDLE = 0x8,
    /** DMA内存 */
    CODEC_BUFFER_TYPE_DMA_MEM_FD = 0x10,
};

/**
 * @brief 查询vendor层支持buffer类型信息
 */
struct SupportBufferType {
    unsigned int  size;              /**< 结构体大小 */
    union CodecVersionType  version; /**< 组件版本 */
    unsigned int portIndex;          /**< 端口序列 */
    unsigned int bufferTypes;        /**< 支持的buffer类型 */
};

/**
 * @brief 设置输入输出端口对应的buffer类型
 */
struct UseBufferType {
    unsigned int size;              /**< 结构体大小 */
    union CodecVersionType version; /**< 组件版本 */
    unsigned int portIndex;         /**< 端口序列 */
    unsigned int bufferType;        /**< buffer类型 */
};

/**
 * @brief 查询vendor层BufferHandle的默认usage配置
 */
struct GetBufferHandleUsageParams {
    unsigned int size;              /**< 结构体大小 */
    union CodecVersionType version; /**< 组件版本 */
    unsigned int portIndex;         /**< 端口序列 */
    unsigned long usage;            /**< Usage */
};

/**
 * @brief 设置输入输出端口的编解码格式
 */
struct CodecVideoPortFormatParam {
    unsigned int size;                /**< 结构体大小 */
    union CodecVersionType version;   /**< 组件版本 */
    unsigned int portIndex;           /**< 端口序列 */
    unsigned int codecColorIndex;     /**< 已废弃 */
    unsigned int codecColorFormat;    /**< 像素格式 */
    unsigned int codecCompressFormat; /**< 压缩格式  */
    unsigned int framerate;           /**< Q16 format */
};

/**
 * @brief 控制编码画面质量参数
 */
struct ControlRateConstantQuality {
    unsigned int size;              /**< 结构体大小 */
    union CodecVersionType version; /**< 组件版本 */
    unsigned int portIndex;         /**< 端口序列 */
   unsigned int qualityValue;       /**< 画面质量参数 */
};

/**
 * @brief 查询/设置vendor层编解码器工作频率
 */
struct WorkingFrequencyParam {
    unsigned int size;              /**< 结构体大小 */
    union CodecVersionType version; /**< 组件版本 */
    unsigned int level;             /**< 工作频率级别 */
};

/**
 * @brief 设置调用者进程名
 */
struct ProcessNameParam {
    unsigned int size;              /**< 结构体大小 */
    union CodecVersionType version; /**< 组件版本 */
    String processName;             /**< 进程名 */
};

/**
 * @brief 编解码器参数索引，对OMX原生枚举OMX_INDEXTYPE的扩展
 */
enum CodecIndexExType {
    /** Extended BufferType index, value = Codec_IndexExtBufferTypeStartUnused + 0x00a00000 */
    Codec_IndexExtBufferTypeStartUnused = 0x6F000000 + 0x00a00000,
    /** SupportBuffer */
    Codec_IndexParamSupportBufferType,
    /** UseBuffer */
    Codec_IndexParamUseBufferType,
    /** GetBufferHandleUsage */
    Codec_IndexParamGetBufferHandleUsage,
    /** CodecVideoPortFormatParam */
    Codec_IndexCodecVideoPortFormat,
    /** ControlRateConstantQuality */
    Codec_IndexParamControlRateConstantQuality,
    /** Codec_IndexParamVideoHevc */
    Codec_IndexParamVideoHevc = 0x6F000000 + 0x00a00007,
    /** range/primary/transfer/matrix */
    Codec_IndexColorAspects,
    /** WorkingFrequencyParam */
    Codec_IndexParamWorkingFrequency,
    /** ProcessNameParam */
    Codec_IndexParamProcessName,
};

/**
 * @brief 定义HEVC视频编码格式信息
 */
struct CodecVideoParamHevc {
    unsigned int size;              /**< 结构体大小 */
    union CodecVersionType version; /**< 组件版本 */
    unsigned int portIndex;         /**< 端口序列 */
    enum CodecHevcProfile profile;  /**< Hevc profile。 详见 {@link CodecHevcProfile}. */
    enum CodecHevcLevel level;      /**< Hevc级别。 详见 {@link CodecHevcLevel}. */
    unsigned int keyFrameInterval;  /**< 连续I帧（包括其中一个I帧）之间的距离。
                                         0表示间隔未指定，可由编解码器自由选择，
                                         1表示一个仅存在I帧的流，其他值表示实际值。 */
};

/**
 * @brief 视频像素值范围
 */
enum RangeType {
    RANGE_UNSPECIFIED,               /**< 未指定的range类型 */
    RANGE_FULL,                      /**< full range */
    RANGE_LIMITED,                   /**< limited range */
    RANGE_MAX = 0xff,                /**< 最大值 */
};

/**
 * @brief 设置视频色域
 */
enum Primaries {
    PRIMARIES_UNSPECIFIED,
    PRIMARIES_BT709,       //Rec. ITU-R BT.709-6
    PRIMARIES_BT470_6M,    //Rec. ITU-R BT.470-6 System M
    PRIMARIES_BT601_625,   //Rec. ITU-R BT.601-7 625 or Rec. ITU-R BT.470-6 System B,G
    PRIMARIES_BT601_525,   //Rec. ITU-R BT.601-7 525 or SMPTE ST 170 or SMPTE ST 240
    PRIMARIES_GENERICFILM, //Generic Film
    PRIMARIES_BT2020,      //Rec. ITU-R BT.2020-2 or Rec. ITU-R BT.2100-2
    PRIMARIES_MAX = 0xff,  //最大值
};

/**
 * @brief 设置视频传递函数
 */
enum Transfer {
    TRANSFER_UNSPECIFIED,
    TRANSFER_LINEAR,          //线性传递特性
    TRANSFER_SRGB,            //IEC 61966-2-1 sRGB
    TRANSFER_SMPTE170,        //SMPTE ST 170 or Rec. ITU-R BT.709-6 or BT.601-7 or BT.2020-2
    TRANSFER_GAMMA22,         //Rec. ITU-R BT.470-6 System M
    TRANSFER_GAMMA28,         //Rec. ITU-R BT.470-6 System B,G
    TRANSFER_PQ,              //Rec. ITU-R BT.2100-2 perceptual quantization (PQ) system
    TRANSFER_HLG,             //Rec. ITU-R BT.2100-2 hybrid log gamma (HLG) system
    TRANSFER_SMPTE240 = 0x40, //SMPTE ST 240
    TRANSFER_XVYCC,           //IEC 61966-2-4
    TRANSFER_BT1361,          //Rec. ITU-R BT.1361-0 extended colour gamut system
    TRANSFER_ST428,           //SMPTE ST 428-1
    TRANSFER_MAX = 0xff,      //最大值
};

/**
 * @brief 设置视频矩阵系数
 */
enum MatrixCoeffs {
    MATRIX_UNSPECIFED,
    MATRIX_BT709,          //Rec. ITU-R BT.709-6
    MATRIX_FCC,            //United States Federal Communications Commission
    MATRIX_BT601,          //Rec. ITU-R BT.601-7 or Rec. ITU-R BT.470-6 System B,G
    MATRIX_SMPTE240,       //SMPTE ST 240
    MATRIX_BT2020,         //Rec. ITU-R BT.2100-2 (非恒定亮度)
    MATRIX_BT2020CONSTANT, //Rec. ITU-R BT.2100-2 (恒定亮度)
    MATRIX_MAX = 0xff,     //最大值
};

/**
 * @brief 色彩空间相关枚举
 */
struct ColorAspects {
    enum RangeType range;                /**< 视频像素值范围 */
    enum Primaries primaries;            /**< 视频色域 */
    enum Transfer transfer;              /**< 视频传递函数 */
    enum MatrixCoeffs matrixCoeffs;      /**< 视频矩阵系数 */
};

/**
 * @brief 定义色彩空间参数信息
 */
struct CodecVideoColorspace {
    unsigned int size;                /**< 结构体大小 */
    union CodecVersionType version;   /**< 组件版本 */
    unsigned int portIndex;           /**< 端口序列 */
    unsigned int requestingDataSpace; /**< 请求颜色空间信息 */
    unsigned int dataSpaceChanged;    /**< 颜色空间信息发生变化 */
    unsigned int pixeFormat;          /**< 像素格式 */
    unsigned int dataSpace;           /**< 颜色空间 */
    struct ColorAspects aspects;      /**< 色彩空间 */
};