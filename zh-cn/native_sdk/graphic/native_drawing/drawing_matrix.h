/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_MATRIX_H
#define C_INCLUDE_DRAWING_MATRIX_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_matrix.h
 *
 * @brief 文件中定义了与矩阵相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_matrix.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于创建一个矩阵对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的矩阵对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreate(void);

/**
 * @brief 创建一个带旋转属性的矩阵对象。
 * 该矩阵对象为：单位矩阵在(x, y)旋转点以度为单位进行旋转。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param deg  旋转的角度，单位为度。正数表示按顺时针旋转，负数表示按逆时针旋转。
 * @param x  x轴上坐标点。
 * @param y  y轴上坐标点。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateRotation(float deg, float x, float y);

/**
 * @brief 创建一个带缩放属性的矩阵对象。
 * 该矩阵对象为：单位矩阵在(px, py)旋转点以sx和sy为缩放因子进行缩放。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sx  水平缩放因子。
 * @param sy  垂直缩放因子。
 * @param px  x轴上坐标点。
 * @param py  y轴上坐标点。
 * @return 函数返回一个指针，指针指向创建的矩阵对象{@link OH_Drawing_Matrix}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateScale(float sx, float sy, float px, float py);

/**
 * @brief 创建一个带平移属性的矩阵对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param dx  水平方向平移距离。
 * @param dy  垂直方向平移距离。
 * @return 函数返回一个指针，指针指向创建的矩阵对象{@link OH_Drawing_Matrix}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateTranslation(float dx, float dy);

/**
 * @brief 用于给矩阵对象设置参数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象的指针。
 * @param scaleX  水平缩放系数。
 * @param skewX   水平倾斜系数。
 * @param transX  水平位移系数。
 * @param skewY   垂直倾斜系数。
 * @param scaleY  垂直缩放系数。
 * @param transY  垂直位移系数。
 * @param persp0  X轴透视系数。
 * @param persp1  Y轴透视系数。
 * @param persp2  透视缩放系数。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MatrixSetMatrix(OH_Drawing_Matrix*, float scaleX, float skewX, float transX,
    float skewY, float scaleY, float transY, float persp0, float persp1, float persp2);

/**
 * @brief 将矩阵total设置为矩阵a乘以矩阵b。
 *       例如给定矩阵a和矩阵b如下所示:
 *                    | A B C |          | J K L |
 *                a = | D E F |,     b = | M N O |
 *                    | G H I |          | P Q R |
 *       设置的最终矩阵total为:
 *                            | A B C |   | J K L |   | AJ+BM+CP AK+BN+CQ AL+BO+CR |
 *           total = a * b =  | D E F | * | M N O | = | DJ+EM+FP DK+EN+FQ DL+EO+FR |
 *                            | G H I |   | P Q R |   | GJ+HM+IP GK+HN+IQ GL+HO+IR |
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param total 指向最终的矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param a 指向矩阵对象a{@link OH_Drawing_Matrix}的指针。
 * @param b 指向矩阵对象b{@link OH_Drawing_Matrix}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixConcat(OH_Drawing_Matrix* total, const OH_Drawing_Matrix* a,
    const OH_Drawing_Matrix* b);

/**
 * @brief 获取矩阵给定索引位的值。索引范围0-8。
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param index 索引位置，范围0-8。
 * @return 函数返回矩阵给定索引位对应的值。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_MatrixGetValue(OH_Drawing_Matrix*, int index);

/**
 * @brief 设置矩阵为单位矩阵，并围绕位于(px, py)的旋转轴点进行旋转。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param degree 角度，单位为度。正数表示顺时针旋转，负数表示逆时针旋转。
 * @param px x轴上坐标点。
 * @param py y轴上坐标点。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixRotate(OH_Drawing_Matrix*, float degree, float px, float py);

/**
 * @brief 设置矩阵为单位矩阵，并平移(dx, dy)。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param dx 水平方向平移距离。
 * @param dy 垂直方向平移距离。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixTranslate(OH_Drawing_Matrix*, float dx, float dy);

/**
 * @brief 设置矩阵为单位矩阵，并围绕位于(px, py)的旋转轴点，以sx和sy进行缩放。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param sx 水平缩放因子。
 * @param sy 垂直缩放因子。
 * @param px x轴上坐标点。
 * @param py y轴上坐标点。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixScale(OH_Drawing_Matrix*, float sx, float sy, float px, float py);

/**
 * @brief 将矩阵inverse设置为矩阵的倒数，并返回结果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param inverse 指向逆矩阵对象{@link OH_Drawing_Matrix}的指针，
 * 开发者可调用{@link OH_Drawing_MatrixCreate}接口创建。
 * @return 函数返回true表示矩阵可逆，inverse被填充为逆矩阵；函数返回false表示矩阵不可逆，inverse不被改变。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixInvert(OH_Drawing_Matrix*, OH_Drawing_Matrix* inverse);

/**
 * @brief 判断两个矩阵是否相等。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向用于判断的其中一个矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param other 指向用于判断的另一个矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @return 函数返回两个矩阵的比较结果，返回true表示两个矩阵相等，返回false表示两个矩阵不相等。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixIsEqual(OH_Drawing_Matrix*, OH_Drawing_Matrix* other);

/**
 * @brief 判断矩阵是否是单位矩阵。
 * 单位矩阵为 :  | 1 0 0 |
 *              | 0 1 0 |
 *              | 0 0 1 |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @return 函数返回true表示矩阵是单位矩阵，函数返回false表示矩阵不是单位矩阵。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixIsIdentity(OH_Drawing_Matrix*);

/**
 * @brief 用于销毁矩阵对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向字体对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MatrixDestroy(OH_Drawing_Matrix*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
