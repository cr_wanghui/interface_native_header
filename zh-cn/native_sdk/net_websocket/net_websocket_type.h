/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NET_WEBSOCKET_TYPE_H
#define NET_WEBSOCKET_TYPE_H

/**
 * @addtogroup netstack
 * @{
 *
 * @brief  为websocket客户端模块提供C接口
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file net_websocket_type.h
 * @brief 定义websocket客户端模块的C接口需要的数据结构
 *
 * @library libnet_websocket.so
 * @syscap SystemCapability.Communication.NetStack
 * @since 11
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief websocket客户端来自服务端关闭的参数
 *
 * @since 11
 * @version 1.0
 */
struct WebSocket_CloseResult {
    /** 关闭的错误码 */
    uint32_t code;
    /** 关闭的错误原因 */
    const char *reason;
};

/**
 * @brief websocket客户端主动关闭的参数
 *
 * @since 11
 * @version 1.0
 */
struct WebSocket_CloseOption {
    /** 关闭的错误码 */
    uint32_t code;
    /** 关闭的错误原因 */
    const char *reason;
};

/**
 * @brief websocket客户端来自服务端连接错误的参数
 *
 * @since 11
 * @version 1.0
 */
struct WebSocket_ErrorResult {
    /** 错误码 */
    uint32_t errorCode;
    /** 错误的消息 */
    const char *errorMessage;
};

/**
 * @brief websocket客户端来自服务端连接成功的参数
 *
 * @since 11
 * @version 1.0
 */
struct WebSocket_OpenResult {
    /** websocket客户端连接成功码 */
    uint32_t code;
    /** websocket客户端连接原因 */
    const char *reason;
};

/**
 * @brief  websocket客户端接收open消息的回调函数定义
 *
 * @param client websocket客户端
 * @param openResult   websocket客户端接收建立连接消息的内容
 * @since 11
 * @version 1.0
 */
typedef void (*WebSocket_OnOpenCallback)(struct WebSocket *client, WebSocket_OpenResult openResult);

/**
 * @brief  websocket客户端接收数据的回调函数定义
 *
 * @param client websocket客户端
 * @param data   websocket客户端接收的数据
 * @param length websocket客户端接收的数据长度
 * @since 11
 * @version 1.0
 */
typedef void (*WebSocket_OnMessageCallback)(struct WebSocket *client, char *data, uint32_t length);

/**
 * @brief  websocket客户端接收error错误消息的回调函数定义
 *
 * @param client websocket客户端
 * @param errorResult   websocket客户端接收连接错误消息的内容
 * @since 11
 * @version 1.0
 */
typedef void (*WebSocket_OnErrorCallback)(struct WebSocket *client, WebSocket_ErrorResult errorResult);

/**
 * @brief  websocket客户端接收close消息的回调函数定义
 *
 * @param client websocket客户端
 * @param closeResult   websocket客户端接收关闭消息的内容
 * @since 11
 * @version 1.0
 */
typedef void (*WebSocket_OnCloseCallback)(struct WebSocket *client, WebSocket_CloseResult closeResult);

/**
 * @brief  websocket客户端增加header头的链表节点
 *
 * @since 11
 * @version 1.0
 */
struct WebSocket_Header {
    /** header头的字段名 */
    const char *fieldName;
    /**header头的字段内容 */
    const char *fieldValue;
    /** header头链表的next指针 */
    struct WebSocket_Header *next;
};

/**
 * @brief  websocket客户端和服务端建立连接的参数
 *
 * @param headers header头信息
 * @since 11
 * @version 1.0
 */
struct WebSocket_RequestOptions {
    struct WebSocket_Header *headers;
};

/**
 * @brief  websocket客户端结构体
 *
 * @since 11
 * @version 1.0
 */
struct WebSocket {
    /** 客户端接收连接消息的回调指针 */
    WebSocket_OnOpenCallback onOpen;
    /**客户端接收消息的回调指针 */
    WebSocket_OnMessageCallback onMessage;
    /** 客户端接收错误消息的回调指针 */
    WebSocket_OnErrorCallback onError;
    /** 客户端接收关闭消息的回调指针 */
    WebSocket_OnCloseCallback onClose;
    /** 客户端建立连接请求内容 */
    WebSocket_RequestOptions requestOptions;
};

/**
 * @brief  websocket错误码
 *
 * @since 11
 * @version 1.0
 */
typedef enum WebSocket_ErrCode {
    /**
     * 执行成功
     */
    WEBSOCKET_OK = 0,

    /**
     * @brief 异常错误代码的基础
     */
    E_BASE = 1000,

    /**
     * @brief websocket为空
     */
    WEBSOCKET_CLIENT_NULL = (E_BASE + 1),

    /**
     * @brief websocket未创建
     */
    WEBSOCKET_CLIENT_NOT_CREATED = (E_BASE + 2),

    /**
     * @brief websocket客户端连接错误
     */
    WEBSOCKET_CONNECTION_ERROR = (E_BASE + 3),

    /**
     * @brief websocket客户端连接参数解析错误
     */
    WEBSOCKET_CONNECTION_PARSE_URL_ERROR = (E_BASE + 5),

    /**
     * @brief websocket客户端连接时创建上下文无内存
     */
    WEBSOCKET_CONNECTION_NO_MEMORY = (E_BASE + 6),

    /**
     * @brief 初始化时候关闭
     */
    WEBSOCKET_CONNECTION_CLOSED_BY_PEER = (E_BASE + 7),

    /**
     * @brief websocket连接被销毁
     */
    WEBSOCKET_DESTROYED = (E_BASE + 8),

    /**
     * @brief websocket客户端连接时候协议错误
     */
    WEBSOCKET_PROTOCOL_ERROR = (E_BASE + 9),

    /**
     * @brief websocket客户端发送数据时候没有足够内存
     */
    WEBSOCKET_SEND_NO_MEMORY = (E_BASE + 10),

    /**
     * @brief websocket客户端发送数据为空
     */
    WEBSOCKET_SEND_DATA_NULL = (E_BASE + 11),

    /**
     * @brief websocket客户端发送数据长度超限制
     */
    WEBSOCKET_DATA_LENGTH_EXCEEDED = (E_BASE + 12),

    /**
     * @brief websocket客户端发送数据队列长度超限制
     */
    WEBSOCKET_QUEUE_LENGTH_EXCEEDED = (E_BASE + 13),

    /**
     * @brief websocket客户端上下文为空
     */
    WEBSOCKET_NO_CLIENT_CONTEXT = (E_BASE + 14),

    /**
     * @brief websocket客户端header头异常
     */
    WEBSOCKET_NO_HEADER_CONTEXT = (E_BASE + 15),

    /**
     * @brief websocket客户端header头超过限制
     */
    WEBSOCKET_HEADER_EXCEEDED = (E_BASE + 16),

    /**
     * @brief websocket客户端没有连接
     */
    WEBSOCKET_NO_CONNECTION = (E_BASE + 17),

    /**
     * @brief websocket客户端没有连接上下文
     */
    WEBSOCKET_NO_CONNECTION_CONTEXT = (E_BASE + 18),
} WebSocket_ErrCode;

#ifdef __cplusplus
}
#endif

#endif // NET_WEBSOCKET_TYPE_H
