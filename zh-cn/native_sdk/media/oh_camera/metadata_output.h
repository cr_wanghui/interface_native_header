/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file metadata_output.h
 *
 * @brief 声明元数据输出概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_METADATAOUTPUT_H
#define NATIVE_INCLUDE_CAMERA_METADATAOUTPUT_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 元数据输出对象
 *
 * 可以使用{@link OH_CameraManager_CreateMetadataOutput}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_MetadataOutput Camera_MetadataOutput;

/**
 * @brief 在{@link MetadataOutput_Callbacks}中被调用的元数据输出元数据对象可用回调。
 *
 * @param metadataOutput 传递回调的{@link Camera_MetadataOutput}。
 * @param metadataObject {@link Camera_MetadataObject}将由回调传递。
 * @param size 元数据对象的大小。
 * @since 11
 */
typedef void (*OH_MetadataOutput_OnMetadataObjectAvailable)(Camera_MetadataOutput* metadataOutput,
    Camera_MetadataObject* metadataObject, uint32_t size);

/**
 * @brief 在{@link MetadataOutput_Callbacks}中被调用的元数据输出错误回调。
 *
 * @param metadataOutput 传递回调的{@link Camera_MetadataOutput}。
 * @param errorCode 元数据输出的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_MetadataOutput_OnError)(Camera_MetadataOutput* metadataOutput, Camera_ErrorCode errorCode);

/**
 * @brief 元数据输出的回调。
 *
 * @see OH_MetadataOutput_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct MetadataOutput_Callbacks {
    /**
     * 此回调将调用元数据输出结果数据。
     */
    OH_MetadataOutput_OnMetadataObjectAvailable onMetadataObjectAvailable;

    /**
     * 元数据输出错误事件。
     */
    OH_MetadataOutput_OnError onError;
} MetadataOutput_Callbacks;

/**
 * @brief 注册元数据输出更改事件回调。
 *
 * @param metadataOutput {@link Camera_MetadataOutput}实例。
 * @param callback 要注册的{@link MetadataOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_MetadataOutput_RegisterCallback(Camera_MetadataOutput* metadataOutput,
    MetadataOutput_Callbacks* callback);

/**
 * @brief 注销元数据输出更改事件回调。
 *
 * @param metadataOutput {@link Camera_MetadataOutput}实例。
 * @param callback 要注销的{@link MetadataOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_MetadataOutput_UnregisterCallback(Camera_MetadataOutput* metadataOutput,
    MetadataOutput_Callbacks* callback);

/**
 * @brief 启动元数据输出。
 *
 * @param metadataOutput 要启动的{@link Camera_MetadataOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_MetadataOutput_Start(Camera_MetadataOutput* metadataOutput);

/**
 * @brief 停止元数据输出。
 *
 * @param metadataOutput 要停止的{@link Camera_MetadataOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_MetadataOutput_Stop(Camera_MetadataOutput* metadataOutput);

/**
 * @brief 释放元数据输出。
 *
 * @param metadataOutput 要释放的{@link Camera_MetadataOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_MetadataOutput_Release(Camera_MetadataOutput* metadataOutput);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_METADATAOUTPUT_H
/** @} */